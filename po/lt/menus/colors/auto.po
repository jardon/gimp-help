#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-03-05 19:42+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  autostretch-hsv.po (PACKAGE VERSION)  #-#-#-#-#\n"
"#-#-#-#-#  c-astretch.po (PACKAGE VERSION)  #-#-#-#-#\n"
"#-#-#-#-#  color-enhance.po (PACKAGE VERSION)  #-#-#-#-#\n"
"#-#-#-#-#  equalize.po (PACKAGE VERSION)  #-#-#-#-#\n"
"#-#-#-#-#  normalize.po (PACKAGE VERSION)  #-#-#-#-#\n"
"#-#-#-#-#  white-balance.po (PACKAGE VERSION)  #-#-#-#-#\n"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/white-balance.xml:69(None)
#: src/menus/colors/auto/stretch-contrast.xml:120(None)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:55(None)
#: src/menus/colors/auto/equalize.xml:54(None)
#: src/menus/colors/auto/color-enhance-legacy.xml:59(None)
msgid ""
"@@image: 'images/menus/colors/auto/alice.png'; "
"md5=a33d190d14dbff2cc22559afe586614b"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/white-balance.xml:84(None)
msgid ""
"@@image: 'images/menus/colors/auto/white-balance.png'; "
"md5=047e4fec700babd6f8a2f9a3be6b8c6f"
msgstr ""

#: src/menus/colors/auto/white-balance.xml:10(title)
#: src/menus/colors/auto/white-balance.xml:17(primary)
msgid "White Balance"
msgstr "Baltumo balansas"

#: src/menus/colors/auto/white-balance.xml:13(primary)
#: src/menus/colors/auto/stretch-contrast.xml:14(primary)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:13(primary)
#: src/menus/colors/auto/equalize.xml:12(primary)
#: src/menus/colors/auto/color-enhance.xml:9(primary)
#: src/menus/colors/auto/color-enhance-legacy.xml:12(primary)
msgid "Colors"
msgstr ""

#: src/menus/colors/auto/white-balance.xml:14(secondary)
msgid "White balance"
msgstr ""

#: src/menus/colors/auto/white-balance.xml:20(para)
#, fuzzy
msgid ""
"The <guimenuitem>White Balance</guimenuitem> command automatically adjusts "
"the colors of the active layer by stretching the Red, Green and Blue "
"channels separately. To do this, it discards pixel colors at each end of the "
"Red, Green and Blue histograms which are used by only 0.05% of the pixels in "
"the image and stretches the remaining range as much as possible. The result "
"is that pixel colors which occur very infrequently at the outer edges of the "
"histograms (perhaps bits of dust, etc.) do not negatively influence the "
"minimum and maximum values used for stretching the histograms, in comparison "
"with <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</link>. "
"Like <quote>Stretch Contrast</quote>, however, there may be hue shifts in "
"the resulting image."
msgstr ""
"Komanda <guimenuitem>Baltumo balansas</guimenuitem> automatiškai pakoreguoja "
"aktyvaus sluoksnio spalvas atskirai ištempiant raudoną, žalią ir mėlyną "
"kanalus. Kad tą padarytų, ši komanda pašalina pikselių spalvas kiekviename "
"raudonos, žalios ir mėlynos histogramų galuose, kurios yra naudojamos tik "
"0,05% paveikslėlių pikselių ir kiek įmanoma ištempia likusį diapazoną. "
"Gautas rezultatas naudingas tuo, kad pikselių spalvos, kurios išoriniuose "
"histogramų galuose pasitaiko labai dažnai (galbūt tai dulkės ir t. t.), "
"neigiamai neįtakoja mažiausios ir didžiausios reikšmių, naudojamų histogramų "
"ištempimui, palyginti su komanda <link linkend=\"gimp-filter-stretch-"
"contrast\">Ištempti kontrastą</link>. Tačiau, kaip ir naudojant komanda "
"<quote>Ištempti kontrastą</quote>, gautame paveikslėlyje gali būti atspalvio "
"pakitimų."

#: src/menus/colors/auto/white-balance.xml:34(para)
msgid ""
"This command suits images with poor white or black. Since it tends to create "
"pure white (and black), it may be useful e.g. to enhance photographs."
msgstr ""
"Ši komanda tinka paveikslėliams, kuriuose yra nešvari balta arba juoda "
"spalvos. Kadangi ji paprastai sukuria gryną baltą (arba juodą), ji gali būti "
"naudinga, pvz., norint pagerinti nuotraukas."

#: src/menus/colors/auto/white-balance.xml:40(para)
msgid ""
"This command only works on RGB images. If the image is Grayscale or Indexed, "
"the menu entry is disabled."
msgstr ""

#: src/menus/colors/auto/white-balance.xml:47(title)
#: src/menus/colors/auto/stretch-contrast.xml:44(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:33(title)
#: src/menus/colors/auto/equalize.xml:36(title)
msgid "Activate the Command"
msgstr "Komandos aktyvavimas"

#: src/menus/colors/auto/white-balance.xml:50(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>White Balance</guimenuitem></menuchoice>."
msgstr ""
"Šią komandą galite aktyvuoti paveikslėlio meniu juostoje: "
"<menuchoice><guimenu>Sp<accel>a</accel>lvos</guimenu><guisubmenu><accel>A</"
"accel>utomatinis</guisubmenu><guimenuitem><accel>B</accel>altumo balansas</"
"guimenuitem></menuchoice>."

#: src/menus/colors/auto/white-balance.xml:63(title)
msgid "<quote>White Balance</quote> example"
msgstr "Komandos <quote>Baltumo balansas</quote> pavyzdys"

#: src/menus/colors/auto/white-balance.xml:65(title)
#: src/menus/colors/auto/stretch-contrast.xml:116(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:51(title)
#: src/menus/colors/auto/equalize.xml:50(title)
#: src/menus/colors/auto/color-enhance-legacy.xml:55(title)
msgid "Original image"
msgstr "Originalus paveikslėlis"

#: src/menus/colors/auto/white-balance.xml:72(para)
msgid ""
"The active layer and its Red, Green and Blue histograms before <quote>White "
"Balance</quote>."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia bei mėlyna histogramos prieš įvykdant "
"komandą <quote>Baltumo balansas</quote>."

#: src/menus/colors/auto/white-balance.xml:80(title)
#: src/menus/colors/auto/stretch-contrast.xml:131(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:66(title)
#: src/menus/colors/auto/equalize.xml:65(title)
#, fuzzy
msgid "Image after the command"
msgstr ""
"#-#-#-#-#  autostretch-hsv.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Paveikslėlis po komandos\n"
"#-#-#-#-#  c-astretch.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Paveikslėlis įvykdžius komandą\n"
"#-#-#-#-#  equalize.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Paveikslėlis įvykdžius komandą\n"
"#-#-#-#-#  normalize.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Paveikslėlis įvykdžius šią komandą\n"
"#-#-#-#-#  white-balance.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Paveikslėlis įvykdžius komandą"

#: src/menus/colors/auto/white-balance.xml:87(para)
msgid ""
"The active layer and its Red, Green and Blue histograms after <quote>White "
"Balance</quote>. Poor white areas in the image became pure white."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia bei mėlyna histogramos įvykdžius "
"komandą <quote>Baltumo balansas</quote>. Paveikslėlyje esančios nešvarios "
"baltos spalvos sritys tampa grynos baltos spalvos."

#: src/menus/colors/auto/white-balance.xml:92(para)
#: src/menus/colors/auto/stretch-contrast.xml:145(para)
#, fuzzy
msgid ""
"Histogram stretching creates gaps between the pixel columns, giving it a "
"striped look."
msgstr ""
"#-#-#-#-#  c-astretch.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Ištempus histogramą gaunami tarpai tarp pikselių stulpelių, suteikiant "
"juostuotą vaizdą.\n"
"#-#-#-#-#  normalize.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Ištempiant histogramą tarp pikselių stulpelių atsiranda tarpų, suteikiančių "
"jai juostuotą išvaizdą.\n"
"#-#-#-#-#  white-balance.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Ištempiant histogramą tarp pikselių stulpelių atsiranda tarpų, suteikiančių "
"jai juostuotą išvaizdą."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/stretch-contrast.xml:66(None)
msgid ""
"@@image: 'images/menus/colors/auto/stretch_contrast-dialog.png'; "
"md5=02a986c28ff64cf864c3d60d4525b8fd"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/stretch-contrast.xml:135(None)
msgid ""
"@@image: 'images/menus/colors/auto/c-stretch.png'; "
"md5=7af9a55590071580eef4731b51cef312"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:11(title)
#: src/menus/colors/auto/stretch-contrast.xml:18(primary)
msgid "Stretch Contrast"
msgstr "Ištempti kontrastą"

#: src/menus/colors/auto/stretch-contrast.xml:15(secondary)
msgid "Stretch contrast"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:21(primary)
msgid "Contrast"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:24(para)
#, fuzzy
msgid ""
"The <guimenuitem>Stretch Contrast</guimenuitem> command automatically "
"stretches the histogram values in the active layer. For each channel of the "
"active layer, it finds the minimum and maximum values and uses them to "
"stretch the Red, Green and Blue histograms to the full contrast range. The "
"bright colors become brighter and the dark colors become darker, which "
"increases the contrast. <quote>Stretch Contrast</quote> works on layers of "
"RGB, Grayscale and Indexed images. Use <quote>Stretch Contrast</quote> only "
"if you want to remove an undesirable color tint from an image which should "
"contain pure white and pure black."
msgstr ""
"Komanda <guimenuitem>Ištempti konrastą</guimenuitem> automatiškai ištempia "
"aktyvaus sluoksnio histogramos reikšmes. Kiekviename aktyvaus sluoksnio "
"kanale ji randa mažiausią bei didžiausią reikšmes ir naudoja jas ištempiant "
"raudoną, žalią ir mėlyną histogramas iki viso kontrasto diapazono. Šviesios "
"spalvos tampa šviesesnės, o tamsios spalvos tampa tamsesnės, todėl padidėja "
"kontrastas. Ši komanda sukuria panašų efektą kaip komanda <link "
"linkend=\"plug-in-normalize\">Normalizuoti</link>, bet nuo jos skiriasi tuo, "
"kad ji veikia kiekviename sluoksnio spalvų kanale atskirai. Dėl to "
"paveikslėlyje paprastai pasislenka spalvos, todėl ji gali neduoti "
"pageidaujamo rezultato. Komanda <quote>Ištempti kontrastą</quote> veikia "
"RGB, pilkų atspalvių ir indeksuotų paveikslėlių sluoksniuose. Komandą "
"<quote>Ištempti kontrastą</quote> naudokite tik jeigu norite pašalinti "
"nepageidaujamą atspalvį iš paveikslėlio, kuriame turėtų būti grynos balta ir "
"juoda spalvos."

#: src/menus/colors/auto/stretch-contrast.xml:36(para)
msgid ""
"This command is also similar to the <link linkend=\"gimp-layer-white-"
"balance\">Color Balance</link> command, but it does not reject any of the "
"very dark or very bright pixels, so the white might be impure."
msgstr ""
"Ši komanda taip pat panaši į komandą <link linkend=\"gimp-layer-white-"
"balance\">Spalvų balansas</link>, tačiau ji neatmeta nei labai tamsių, nei "
"labai šviesių pikselių, todėl balta spalva gali būti negryna."

#: src/menus/colors/auto/stretch-contrast.xml:47(para)
#, fuzzy
msgid ""
"This command can be accessed from an image menubar as "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Stretch Contrast</guimenuitem></menuchoice>."
msgstr ""
"Šią komandą galima aktyvuoti paveikslėlio meniu juostoje: "
"<menuchoice><guimenu>Sp<accel>a</accel>lvos</guimenu><guisubmenu><accel>A</"
"accel>utomatinis</guisubmenu><guimenuitem>Iš<accel>t</accel>empti kontrastą</"
"guimenuitem></menuchoice>."

#: src/menus/colors/auto/stretch-contrast.xml:60(title)
msgid "Options"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:62(title)
#, fuzzy
msgid "<quote>Stretch Contrast</quote> settings"
msgstr "Komandos <quote>Ištempti kontrastą</quote> pavyzdys"

#: src/menus/colors/auto/stretch-contrast.xml:72(term)
msgid "Presets"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:74(para)
msgid ""
"<quote>Presets</quote> are a common feature for several Colors commands. You "
"can find its description in <xref linkend=\"colors-common-features\"/>."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:83(term)
msgid "Keep Colors"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:85(para)
msgid "Impact each color channel with the same amount."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:92(term)
msgid "Non-Linear Components"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:94(para)
msgid ""
"When set, this option operates on gamma corrected values instead of linear "
"RGB, acting like the old Normalize filter."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:102(term)
msgid "Blending Options, Preview and Split view"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:104(para)
msgid ""
"These are common features described in <xref linkend=\"colors-common-"
"features\"/>."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:114(title)
msgid "<quote>Stretch Contrast</quote> Example"
msgstr "Komandos <quote>Ištempti kontrastą</quote> pavyzdys"

#: src/menus/colors/auto/stretch-contrast.xml:123(para)
msgid ""
"The layer and its Red, Green and Blue histograms before <quote>Stretch "
"Contrast</quote>."
msgstr ""
"Sluoksnis ir jo raudona, žalia ir mėlyna histogramos prieš pritaikant "
"komandą <quote>Ištempti kontrastą</quote>."

#: src/menus/colors/auto/stretch-contrast.xml:138(para)
msgid ""
"The layer and its Red and Green and Blue histograms after <quote>Stretch "
"Contrast</quote>. The pixel columns do not reach the right end of the "
"histogram (255) because of a few very bright pixels, unlike <quote>White "
"Balance</quote>."
msgstr ""
"Sluoksnis ir jo raudona, žalia bei mėlyna histogramos įvykdžius komandą "
"<quote>Ištempti kontrastą</quote>. Pikselių stulpeliai nepasiekia dešiniojo "
"histogramos krašto (255) dėl kelių labai šviesių pikselių, skirtingai nei "
"naudojant komandą <quote>Baltumo balansas</quote>."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/stretch-contrast-hsv.xml:70(None)
msgid ""
"@@image: 'images/menus/colors/auto/stretch-hsv.png'; "
"md5=fb016a380256c6bbf3a302281a432904"
msgstr ""

#: src/menus/colors/auto/stretch-contrast-hsv.xml:10(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:17(primary)
msgid "Stretch HSV"
msgstr "Ištempti HSV"

#: src/menus/colors/auto/stretch-contrast-hsv.xml:14(secondary)
msgid "Stretch colors in HSV space"
msgstr ""

#: src/menus/colors/auto/stretch-contrast-hsv.xml:20(para)
#, fuzzy
msgid ""
"The <guimenuitem>Stretch Contrast HSV</guimenuitem> command does the same "
"thing as the <link linkend=\"gimp-filter-stretch-contrast\">Stretch "
"Contrast</link> command, except that it works in HSV color space, rather "
"than RGB color space, and it preserves the Hue. Thus, it independently "
"stretches the ranges of the Hue, Saturation and Value components of the "
"colors. Occasionally the results are good, often they are a bit odd. "
"<quote>Stretch Contrast HSV</quote> operates on layers from RGB and Indexed "
"images. If the image is Grayscale, the menu entry is insensitive and grayed "
"out."
msgstr ""
"Komanda <guimenuitem>Ištempti HSV</guimenuitem> daro tą patį kaip ir komanda "
"<link linkend=\"gimp-filter-stretch-contrast\">Ištempti kontrastą</link>, "
"išskyrus tai, kad ji veikia HSV spalvų erdvėje, o ne RGB spalvų erdvėje, ir "
"išsaugo atspalvį. Taigi ji nepriklausomai ištempia spalvų atspalvio, sodrumo "
"ir reikšmės komponentų rėžius. Retkarčiais rezultatai yra geri, bet dažnai "
"jie būna gan keisti. Komanda <quote>Ištempti HSV</quote> veikia tik RGB ir "
"indeksuotų paveikslėlių sluoksniuose. Jeigu paveikslėlis yra pilkų "
"atspalvių, šis meniu įrašas yra išjungtas."

#: src/menus/colors/auto/stretch-contrast-hsv.xml:36(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Stretch Contrast HSV</guimenuitem></menuchoice>."
msgstr ""
"Šią komandą galite aktyvuoti paveikslėlio meniu juostoje: "
"<menuchoice><guimenu>Sp<accel>a</accel>lvos</guimenu><guisubmenu><accel>A</"
"accel>utomatinis</guisubmenu><guimenuitem>Ištempti <accel>H</accel>SV</"
"guimenuitem></menuchoice>."

#: src/menus/colors/auto/stretch-contrast-hsv.xml:49(title)
#, fuzzy
msgid "<quote>Stretch Contrast HSV</quote> example"
msgstr "Komandos <quote>Ištempti kontrastą</quote> pavyzdys"

#: src/menus/colors/auto/stretch-contrast-hsv.xml:58(para)
#, fuzzy
msgid ""
"The active layer and its Red, Green and Blue histograms before "
"<quote>Stretch Contrast HSV</quote>."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia bei mėlyna histogramos prieš "
"panaudojant <quote>Ištempti HSV</quote>."

#: src/menus/colors/auto/stretch-contrast-hsv.xml:73(para)
#, fuzzy
msgid ""
"The active layer and its Red, Green and Blue histograms after <quote>Stretch "
"Contrast HSV</quote>. Contrast, luminosity and hues are enhanced."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia ir mėlyna histogramos pritaikius "
"komandą <quote>Ištempti HSV</quote>. Pagerintas kontrastas, šviesumas ir "
"atspalviai."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/equalize.xml:69(None)
msgid ""
"@@image: 'images/menus/colors/auto/equalize.png'; "
"md5=2000746549b6f65af80e17d64ff69021"
msgstr ""

#: src/menus/colors/auto/equalize.xml:9(title)
#: src/menus/colors/auto/equalize.xml:13(secondary)
#: src/menus/colors/auto/equalize.xml:16(primary)
msgid "Equalize"
msgstr "Išlyginti"

#: src/menus/colors/auto/equalize.xml:18(para)
msgid ""
"The <guimenuitem>Equalize</guimenuitem> command automatically adjusts the "
"brightness of colors across the active layer so that the histogram for the "
"Value channel is as nearly flat as possible, that is, so that each possible "
"brightness value appears at about the same number of pixels as every other "
"value. You can see this in the histograms in the example below, in that "
"pixel colors which occur frequently in the image are stretched further apart "
"than pixel colors which occur only rarely. The results of this command can "
"vary quite a bit. Sometimes <quote>Equalize</quote> works very well to "
"enhance the contrast in an image, bringing out details which were hard to "
"see before. Other times, the results look very bad. It is a very powerful "
"operation and it is worth trying to see if it will improve your image. It "
"works on layers from RGB and Grayscale images. If the image is Indexed, the "
"menu entry is insensitive and grayed out."
msgstr ""
"Komanda <guimenuitem>Išlyginti</guimenuitem> automatiškai pakoreguoja "
"aktyvaus sluoksnio spalvų šviesumą, kad Reikšmės kanalo histograma būtų kiek "
"įmanoma lygi, t. y., kad kiekviena galima šviesumo reikšmė būtų naudojama "
"beveik tiek pat pikselių, kiek ir kiekviena kita reikšmė. Tai galite matyti "
"žemiau pateiktame pavyzdyje parodytose histogramose; dažnai paveikslėlyje "
"pasitaikančios paveikslėlių spalvos yra labiau ištemptos nuo rečiau "
"pasitaikančių pikselių spalvų. Šios komandos rezultatai gali šiek tiek "
"skirtis. Kartais komanda <quote>Išlyginti</quote> labai gerai pagerina "
"paveikslėlio kontrastą, paryškinant detales, kurias anksčiau buvo sunku "
"pastebėti. Tačiau kartais rezultatas gali būti labai prastas. Tai yra labai "
"galinga operacija, todėl verta pažiūrėti, gal ji pagerins jūsų paveikslėlį. "
"Ji veikia RGB ir pilkų atspalvių paveikslėlių sluoksniuose. Jeigu "
"paveikslėlis yra indeksuotas, šis meniu įrašas yra išjungtas."

#: src/menus/colors/auto/equalize.xml:37(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Equalize</guimenuitem></menuchoice>"
msgstr ""
"Šią komandą galite aktyvuoti paveikslėlio meniu juostoje: "
"<menuchoice><guimenu>Sp<accel>a</accel>lvos</guimenu><guisubmenu><accel>A</"
"accel>utomatinis</guisubmenu><guimenuitem><accel>I</accel>šlyginti</"
"guimenuitem></menuchoice>."

#: src/menus/colors/auto/equalize.xml:48(title)
msgid "<quote>Equalize</quote> example"
msgstr "Komandos <quote>išlyginti</quote> pavyzdys"

#: src/menus/colors/auto/equalize.xml:57(para)
msgid ""
"The active layer and its Red, Green, Blue histograms before <quote>Equalize</"
"quote>."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia ir mėlyna histogramos prieš įvykdant "
"komandą <quote>Išlyginti</quote>."

#: src/menus/colors/auto/equalize.xml:72(para)
msgid "The active layer and its Red, Green, Blue histograms after treatment."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia, mėlyna histogramos po apdorojimo."

#: src/menus/colors/auto/equalize.xml:76(para)
#, fuzzy
msgid ""
"Histogram stretching creates gaps between pixel columns giving it a striped "
"look: colors that occur frequently are stretched."
msgstr ""
"Ištempus histogramą atsiranda tarpai tarp pikselių stulpelių, suteikiantys "
"jai juostuotą išvaizdą."

#: src/menus/colors/auto/color-enhance.xml:6(title)
#: src/menus/colors/auto/color-enhance.xml:13(primary)
msgid "Color Enhance"
msgstr "Spalvų pagerinimas"

#: src/menus/colors/auto/color-enhance.xml:10(secondary)
#, fuzzy
msgid "Color enhance"
msgstr "Spalvų pagerinimas"

#: src/menus/colors/auto/color-enhance.xml:16(para)
msgid ""
"The <guimenuitem>Color Enhance</guimenuitem> command stretches the Chroma "
"range of the colors in the layer to the maximum possible, without altering "
"Hue and Lightness. It does this by converting the colors to <ulink "
"url=\"https://en.wikipedia.org/wiki/"
"CIELAB_color_space#CIEHLC_cylindrical_model\">CIE LCh space</ulink>, then "
"stretching the Chroma range to be as large as possible, and finally "
"converting the colors back to its native color space."
msgstr ""

#: src/menus/colors/auto/color-enhance.xml:26(para)
#: src/menus/colors/auto/color-enhance-legacy.xml:30(para)
msgid ""
"This command does not work on Grayscale images. If the image is Grayscale, "
"the menu entry is disabled."
msgstr ""

#: src/menus/colors/auto/color-enhance.xml:33(title)
#: src/menus/colors/auto/color-enhance-legacy.xml:37(title)
#, fuzzy
msgid "Activate the command"
msgstr "Komandos aktyvavimas"

#: src/menus/colors/auto/color-enhance.xml:36(para)
#, fuzzy
msgid ""
"You can access this command from the main menu through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Color Enhance</guimenuitem></menuchoice>."
msgstr ""
"Šią komandą galite aktyvuoti paveikslėlio meniu juostoje: "
"<menuchoice><guimenu>Sp<accel>a</accel>lvos</guimenu><guisubmenu><accel>A</"
"accel>utomatinis</guisubmenu><guimenuitem><accel>S</accel>palvų pagerinimas</"
"guimenuitem></menuchoice>."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/color-enhance-legacy.xml:76(None)
msgid ""
"@@image: 'images/menus/colors/auto/color-enhance.png'; "
"md5=2f419878c979c9438078646bd21d8f53"
msgstr ""

#: src/menus/colors/auto/color-enhance-legacy.xml:9(title)
#: src/menus/colors/auto/color-enhance-legacy.xml:16(primary)
#, fuzzy
msgid "Color Enhance (legacy)"
msgstr "Spalvų pagerinimas"

#: src/menus/colors/auto/color-enhance-legacy.xml:13(secondary)
#, fuzzy
msgid "Color enhance (legacy)"
msgstr "Spalvų pagerinimas"

#: src/menus/colors/auto/color-enhance-legacy.xml:19(para)
#, fuzzy
msgid ""
"The <guimenuitem>Color Enhance</guimenuitem> command increases the "
"saturation range of the colors in the layer, without altering brightness or "
"hue. It does this by converting the colors to HSV space, measuring the range "
"of saturation values across the image, then stretching this range to be as "
"large as possible, and finally converting the colors back to RGB. It is "
"similar to <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</"
"link>, except that it works in the HSV color space, so it preserves the hue."
msgstr ""
"Komanda <guimenuitem>Spalvų pagerinimas</guimenuitem> padidina sluoksnyje "
"esančių spalvų sodrumo diapazoną, nepakeičiant šviesumo arba atspalvio. Tai "
"padaroma konvertuojant spalvas į HSV erdvę, pamatuojant paveikslėlio sodrumo "
"reikšmių diapazoną, tada kiek įmanoma ištempiant šį diapazoną ir galiausiai "
"konvertuojant spalvas atgal į RGB spalvų erdvę. Šios komandos poveikis "
"panašus į komandos <link linkend=\"gimp-filter-stretch-contrast\">Ištempti "
"kontrastą</link>, išskyrus tai, kad ji veikia HSV spalvų erdvėje, taigi "
"išsaugo atspalvį. Ji veikia RGB ir indeksuotų paveikslėlių sluoksniuose. "
"Jeigu paveikslėlis yra pilkų atspalvių, šis meniu įrašas yra išjungtas."

#: src/menus/colors/auto/color-enhance-legacy.xml:40(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Color Enhance (legacy)</guimenuitem></menuchoice>."
msgstr ""
"Šią komandą galite aktyvuoti paveikslėlio meniu juostoje: "
"<menuchoice><guimenu>Sp<accel>a</accel>lvos</guimenu><guisubmenu><accel>A</"
"accel>utomatinis</guisubmenu><guimenuitem><accel>S</accel>palvų pagerinimas</"
"guimenuitem></menuchoice>."

#: src/menus/colors/auto/color-enhance-legacy.xml:53(title)
#, fuzzy
msgid "<quote>Color Enhance (legacy)</quote> example"
msgstr "<quote>Spalvų pagerinimas</quote> pavyzdys"

#: src/menus/colors/auto/color-enhance-legacy.xml:62(para)
msgid ""
"The active layer and its Red, Green and Blue histograms before <quote>Color "
"Enhance</quote>."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia ir mėlyna histogramos prieš "
"<quote>Spalvų pagerinimą</quote>."

#: src/menus/colors/auto/color-enhance-legacy.xml:70(title)
msgid "Command applied"
msgstr ""

#: src/menus/colors/auto/color-enhance-legacy.xml:79(para)
#, fuzzy
msgid ""
"The active layer and its Red, Green and Blue histograms after <quote>Color "
"Enhance (legacy)</quote>. The result may not always be what you expect."
msgstr ""
"Aktyvus sluoksnis ir jo raudona, žalia ir mėlyna histogramos įvykdžius "
"komandą <quote>Spalvų pagerinimas</quote>. Gautas rezultatas ne visada gali "
"patenkinti jūsų lūkesčius."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: src/menus/colors/auto/color-enhance-legacy.xml:0(None)
msgid "translator-credits"
msgstr ""

#~ msgid ""
#~ "<guimenuitem>White Balance</guimenuitem> operates on layers from RGB "
#~ "images. If the image is Indexed or Grayscale, the menu item is "
#~ "insensitive and grayed out."
#~ msgstr ""
#~ "Komanda <guimenuitem>Baltumo balansas</guimenuitem> veikia RGB "
#~ "paveikslėlių sluoksniuose. Jeigu paveikslėlis yra indeksuotas arba pilkų "
#~ "atspalvių, šis meniu elementas yra išjungtas."

#, fuzzy
#~ msgid ""
#~ "Histogram stretching creates gaps between the pixel columns, giving it a "
#~ "stripped look."
#~ msgstr ""
#~ "#-#-#-#-#  c-astretch.po (PACKAGE VERSION)  #-#-#-#-#\n"
#~ "Ištempus histogramą gaunami tarpai tarp pikselių stulpelių, suteikiant "
#~ "juostuotą vaizdą.\n"
#~ "#-#-#-#-#  normalize.po (PACKAGE VERSION)  #-#-#-#-#\n"
#~ "Ištempiant histogramą tarp pikselių stulpelių atsiranda tarpų, "
#~ "suteikiančių jai juostuotą išvaizdą.\n"
#~ "#-#-#-#-#  white-balance.po (PACKAGE VERSION)  #-#-#-#-#\n"
#~ "Ištempiant histogramą tarp pikselių stulpelių atsiranda tarpų, "
#~ "suteikiančių jai juostuotą išvaizdą."

#~ msgid "Normalize"
#~ msgstr "Normalizuoti"

#~ msgid ""
#~ "The <guimenuitem>Normalize</guimenuitem> command scales the brightness "
#~ "values of the active layer so that the darkest point becomes black and "
#~ "the brightest point becomes as bright as possible, without altering its "
#~ "hue. This is often a <quote>magic fix</quote> for images that are dim or "
#~ "washed out. <quote>Normalize</quote> works on layers from RGB, Grayscale, "
#~ "and Indexed images."
#~ msgstr ""
#~ "Komanda <guimenuitem>Normalizuoti</guimenuitem> ištempia aktyvaus "
#~ "sluoksnio šviesumo reikšmes taip, kad tamsiausias taškas tampa juodu, o "
#~ "šviesiausias taštas tampa kiek įmanoma šviesiu, nepakeičiant jo "
#~ "atspalvio. Tai dažnai yra <quote>stebuklingas pataisymo priemonė</quote> "
#~ "paveikslėliams, kurie yra neryškūs arba išblukę. Komanda "
#~ "<quote>Normalizuoti</quote> veikia RGB, pilkų atspalvių ir indeksuotų "
#~ "paveikslėlių sluoksniuose."

#~ msgid ""
#~ "You can access this command from the image menu bar through "
#~ "<menuchoice><guimenu><accel>C</accel>olors</guimenu><guisubmenu><accel>A</"
#~ "accel>uto</guisubmenu><guimenuitem><accel>N</accel>ormalize</"
#~ "guimenuitem></menuchoice>."
#~ msgstr ""
#~ "Šią komandą galite aktyvuoti paveikslėlio meniu juostoje: "
#~ "<menuchoice><guimenu>Sp<accel>a</accel>lvos</"
#~ "guimenu><guisubmenu><accel>A</accel>utomatinis</"
#~ "guisubmenu><guimenuitem><accel>N</accel>ormalizuoti</guimenuitem></"
#~ "menuchoice>."

#~ msgid "<quote>Normalize</quote>Example"
#~ msgstr "Komandos <quote>Normalizuoti</quote> pavyzdys"

#~ msgid ""
#~ "The active layer and its Red, Green and Blue histograms before "
#~ "<quote>Normalize</quote>."
#~ msgstr ""
#~ "Aktyvus sluoksnis ir jo raudona, žalia bei mėlyna histogramos prieš "
#~ "įvykdant komandą <quote>Normalizuoti</quote>."

#~ msgid ""
#~ "The active layer and its Red, Green and Blue histograms after "
#~ "<quote>Normalize</quote>. The contrast is enhanced."
#~ msgstr ""
#~ "Aktyvus sluoksnis ir jo raudona, žalia bei mėlyna histogramos įvykdžius "
#~ "komandą <quote>Normalizuoti</quote>. Pagerintas kontrastas."

#~ msgid ""
#~ "or by using the keyboard shortcut <keycombo><keycap>Shift</"
#~ "keycap><keycap>Page_Down</keycap></keycombo>."
#~ msgstr ""
#~ "arba naudojant klavišų kombinaciją <keycombo><keycap>Shift</"
#~ "keycap><keycap>Page_Down</keycap></keycombo>."

#~ msgid "<quote>Color Enhance</quote> example (Original image)"
#~ msgstr ""
#~ "<quote>Spalvų pagerinimas</quote> pavyzdys (originalus paveikslėlis)"

#~ msgid "<quote>Color Enhance</quote> example (Image after the command)"
#~ msgstr ""
#~ "<quote>Spalvų pagerinimas</quote> pavyzdys (paveikslėlis įvykdžius "
#~ "komandą)"

#~ msgid "<quote>Stretch HSV</quote> example"
#~ msgstr "<quote>Ištempti HSV</quote> pavyzdys"

#~ msgid "Activate Command"
#~ msgstr "Komandos aktyvavimas"
