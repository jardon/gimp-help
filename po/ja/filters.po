# 島本良太 (SimaMoto,RyōTa) <liangtai.s16@gmail.com>, 2009-2013.
msgid ""
msgstr ""
"Project-Id-Version: GIMP-2.8-HELP\n"
"POT-Creation-Date: 2023-03-05 19:42+0000\n"
"PO-Revision-Date: 2012-06-14 22:47+0900\n"
"Last-Translator: liangtai <liangtai.s16@gmail.com>\n"
"Language-Team: Japanese <gnome-translation@gnome.gr.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/web.xml:26(None)
msgid ""
"@@image: 'images/menus/filters/web.png'; md5=a8b0fa42eca1fc0bed84d13b0788a4a5"
msgstr "localized"

#: src/filters/web.xml:11(title)
msgid "Web Filters"
msgstr "ウェブフィルター"

#: src/filters/web.xml:14(primary) src/filters/introduction.xml:19(primary)
#: src/filters/animation.xml:14(primary)
msgid "Filters"
msgstr "フィルター"

#: src/filters/web.xml:15(secondary)
msgid "Web"
msgstr "ウェブ"

#: src/filters/web.xml:16(tertiary) src/filters/web.xml:20(title)
#: src/filters/introduction.xml:16(title)
#: src/filters/introduction.xml:19(secondary)
#: src/filters/animation.xml:16(tertiary) src/filters/animation.xml:20(title)
msgid "Introduction"
msgstr "あらまし"

#: src/filters/web.xml:22(title)
msgid "The Web filters menu"
msgstr "ウェブフィルターのメニュー"

#: src/filters/web.xml:30(para)
#, fuzzy
msgid ""
"These filters are mostly used on images intended for web sites. The filter "
"<link linkend=\"plug-in-imagemap\">ImageMap</link> is used to add clickable "
"<quote>hot spots</quote> on the image. The filter <link linkend=\"gimp-"
"filter-semi-flatten\">Semi-Flatten</link> is used to simulate semi-"
"transparency in image formats without alpha channel. The <link "
"linkend=\"python-fu-slice\">Slice</link> filter creates HTML tables of "
"sensitive images."
msgstr ""
"これらのフィルターは主にウェブ表示に特化した画像の生成に使います。 <link "
"linkend=\"plug-in-imagemap\">イメージマップ...</link> フィルターは画像上にク"
"リック感応領域を配置するときに使います。 <link linkend=\"gimp-filter-semi-"
"flatten\">半統合</link> フィルターはアルファチャンネルがない画像形式にあたか"
"も半透明のような効果を加えるときに使います。 <link linkend=\"python-fu-"
"slice\">画像分割...</link> フィルターはマウスに反応する画像を <acronym>HTML</"
"acronym> 言語のテーブルを用いて構成するときに使います。"

#: src/filters/render.xml:6(title)
msgid "Rendering Filters"
msgstr "下塗りフィルター"

#: src/filters/noise.xml:6(title)
msgid "Noise Filters"
msgstr "ノイズフィルター"

#: src/filters/map.xml:12(title)
msgid "Map Filters"
msgstr "マップフィルター"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/light-and-shadow.xml:14(None)
#, fuzzy
msgid ""
"@@image: 'images/menus/filters/light-and-shadow.png'; "
"md5=71cfa4478b5aafc7491ef0ab1230c33d"
msgstr "localized"

#: src/filters/light-and-shadow.xml:8(title)
msgid "Light and Shadow Filters"
msgstr "照明と投影フィルター"

#: src/filters/light-and-shadow.xml:10(title)
msgid "The Light and Shadow filters menu"
msgstr "照明と投影フィルターのメニュー"

#: src/filters/introduction.xml:22(primary)
#: src/filters/common-features.xml:163(term)
msgid "Preview"
msgstr "プレビュー"

#: src/filters/introduction.xml:22(secondary)
msgid "Filter"
msgstr "フィルター"

#: src/filters/introduction.xml:25(para)
msgid ""
"A filter is a special kind of tool designed to take an input layer or image, "
"apply a mathematical algorithm to it, and return the input layer or image in "
"a modified format. <acronym>GIMP</acronym> uses filters to achieve a variety "
"of effects and those effects are discussed here."
msgstr ""
"フィルターとは与えられたレイヤーもしくは画像に所定の数式アルゴリズムを適用し"
"て改変させることに特化したツールです。 <acronym>GIMP</acronym> はさまざまな効"
"果のあるフィルターをとり揃えています。 この章はそれぞれの効果について説明しま"
"す。"

#: src/filters/introduction.xml:34(para)
msgid "The filters are divided into several categories:"
msgstr "フィルターはつぎのように分類されています。"

#: src/filters/generic.xml:7(title)
msgid "Generic Filters"
msgstr "汎用フィルター"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/enhance.xml:12(None)
#, fuzzy
msgid ""
"@@image: 'images/menus/filters/enhance.png'; "
"md5=98d06467b14d7e5983e9441043ffa43f"
msgstr "localized"

#: src/filters/enhance.xml:6(title)
msgid "Enhance Filters"
msgstr "強調フィルター"

#: src/filters/enhance.xml:8(title)
msgid "The Enhance filters menu"
msgstr "強調フィルターのメニュー"

#: src/filters/edge-detect.xml:10(title)
msgid "Edge-Detect Filters"
msgstr "輪郭抽出フィルター"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/distort.xml:13(None)
#, fuzzy
msgid ""
"@@image: 'images/menus/filters/distorts.png'; "
"md5=4393f7987758456b36e69e68f41f2d62"
msgstr "localized"

#: src/filters/distort.xml:8(title)
msgid "Distort Filters"
msgstr "変形フィルター"

#: src/filters/distort.xml:10(title)
msgid "The Distort filters menu"
msgstr "変形フィルターのメニュー"

#: src/filters/decor.xml:17(title)
msgid "Decor Filters"
msgstr "装飾フィルター"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/common-features.xml:21(None)
msgid ""
"@@image: 'images/filters/common/filter-options-common.png'; "
"md5=cbf72e576a66d17688a83561ee86a09c"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/common-features.xml:30(None)
msgid ""
"@@image: 'images/filters/common/filter-options-common-clipping.png'; "
"md5=74fb9e9a50df146d7de58467531de806"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/common-features.xml:96(None)
#, fuzzy
msgid ""
"@@image: 'images/filters/use_selection_only.jpg'; "
"md5=1169bd72569d30d1005c03fe3821b908"
msgstr "localized"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/common-features.xml:110(None)
#, fuzzy
msgid ""
"@@image: 'images/filters/use_entire_layer.jpg'; "
"md5=1251cb22b4609bd1ce8570f79dd81c1e"
msgstr "localized"

#: src/filters/common-features.xml:8(title)
msgid "Common Features"
msgstr ""

#: src/filters/common-features.xml:10(para)
msgid ""
"Since <acronym>GIMP</acronym> 2.10, most filters are <acronym>GEGL</acronym> "
"filters. <acronym>GEGL</acronym> is the image processing library used by "
"<acronym>GIMP</acronym>. These filters have several options in common, some "
"of which are only shown under certain conditions."
msgstr ""

#: src/filters/common-features.xml:17(title)
msgid "Common Options of GEGL Filters"
msgstr ""

#: src/filters/common-features.xml:24(para)
msgid "When a selection is active"
msgstr ""

#: src/filters/common-features.xml:33(para)
msgid "When the active layer has an alpha channel and no selection is active"
msgstr ""

#: src/filters/common-features.xml:40(term)
msgid "Presets"
msgstr ""

#: src/filters/common-features.xml:42(para)
msgid ""
"Filter presets are similar to tool presets, in that you can save your "
"favorite settings and recall them when needed. They consist of:"
msgstr ""

#: src/filters/common-features.xml:49(para)
msgid ""
"A <emphasis role=\"bold\">drop down list</emphasis> that shows the current "
"preset and lets you choose a different one."
msgstr ""

#: src/filters/common-features.xml:55(para)
msgid ""
"<guiicon><inlinegraphic fileref=\"images/filters/common/preset-icon-add."
"png\"/></guiicon> An icon to save the current settings as a named preset."
msgstr ""

#: src/filters/common-features.xml:64(para)
msgid ""
"<guiicon><inlinegraphic fileref=\"images/filters/common/preset-icon-manage."
"png\"/></guiicon> An icon that opens a menu with options to import presets "
"from a file, export the current presets to a file, and manage presets."
msgstr ""

#: src/filters/common-features.xml:78(term)
msgid "Input Type"
msgstr ""

#: src/filters/common-features.xml:81(para)
msgid ""
"The input type dropdown list is only visible when a selection is active."
msgstr ""

#: src/filters/common-features.xml:88(para)
msgid ""
"<guilabel>Use the selection as input</guilabel> If this option is selected, "
"the filter only uses pixels inside the selection as input for the filter."
msgstr ""

#: src/filters/common-features.xml:101(para)
msgid ""
"<guilabel>Use the entire layer as input</guilabel> If this option is "
"selected, the input of the filter is the entire layer. The output will only "
"affect the selection. The layer outside the selection remains unchanged."
msgstr ""

#: src/filters/common-features.xml:119(term)
msgid "Clipping"
msgstr ""

#: src/filters/common-features.xml:122(para)
msgid ""
"The clipping dropdown list is only visible when the current layer has an "
"alpha channel, and no selection is active."
msgstr ""

#: src/filters/common-features.xml:127(para)
msgid ""
"This setting determines what to do when the result of this filter is larger "
"than the original layer."
msgstr ""

#: src/filters/common-features.xml:133(para)
msgid ""
"<guilabel>Adjust</guilabel> The layer will be automatically resized as "
"necessary when the filter is applied. This is the default."
msgstr ""

#: src/filters/common-features.xml:140(para)
msgid ""
"<guilabel>Clip</guilabel> The result will be clipped to the layer boundary."
msgstr ""

#: src/filters/common-features.xml:150(term)
msgid "Blending Options"
msgstr ""

#: src/filters/common-features.xml:152(para)
msgid ""
"When you expand this option by clicking the +, you can choose the blend "
"<guilabel>Mode</guilabel> to be used when applying the filter, and the "
"<guilabel>Opacity</guilabel>. These work the same as the Layer <xref "
"linkend=\"gimp-layer-dialog-paint-mode-menu\"/> blending options."
msgstr ""

#: src/filters/common-features.xml:165(para)
msgid ""
"When this option is enabled (default), changes in the filter settings are "
"directly displayed on canvas. They are not applied to the image until you "
"click the <guibutton>OK</guibutton> button."
msgstr ""

#: src/filters/common-features.xml:174(term)
msgid "Split view"
msgstr ""

#: src/filters/common-features.xml:176(para)
msgid ""
"When this option is enabled, the view of the image is divided in two parts. "
"On the left side it shows the effect of the filter applied, and on the right "
"side it shows the image without filter."
msgstr ""

#: src/filters/common-features.xml:182(para)
msgid ""
"You can click-and-drag the line that divides the preview to move it, and "
"<keycap>Ctrl</keycap>-click to make the line horizontal, or to switch it "
"back to vertical."
msgstr ""

#: src/filters/combine.xml:10(title)
msgid "Combine Filters"
msgstr "合成フィルター"

#: src/filters/blur.xml:9(title)
msgid "Blur Filters"
msgstr "ぼかしフィルター"

#: src/filters/artistic.xml:10(title)
msgid "Artistic Filters"
msgstr "芸術的効果フィルター"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/filters/animation.xml:26(None)
msgid ""
"@@image: 'images/menus/filters/animation.png'; "
"md5=22d6f6979f305742cb960f9765f312e5"
msgstr "localized"

#: src/filters/animation.xml:11(title)
msgid "Animation Filters"
msgstr "アニメーションフィルター"

#: src/filters/animation.xml:15(secondary)
msgid "Animation"
msgstr "アニメーション"

#: src/filters/animation.xml:22(title)
msgid "The Animation filters menu"
msgstr "アニメーションフィルターのメニュー"

#: src/filters/animation.xml:30(para)
#, fuzzy
msgid ""
"These are animation helpers, which let you view and optimize your animations "
"(by reducing their size). We gathered <quote>Optimize (Difference)</quote> "
"and <quote>Optimize (for GIF)</quote> filters together, because they are not "
"much different."
msgstr ""
"これらのフィルターはアニメーション画像を軽量化して最適化させるとともに、 鑑賞"
"もできる支援ツールです。 実際は別々の<quote>差分最適化</quote>フィルターと"
"<quote>GIF 最適化</quote>フィルターは、 両者にさほど相違がないことからまとめ"
"て紹介しています。"

#: src/filters/about-common-features.xml:8(para)
msgid "These options are described in <xref linkend=\"gimp-filters-common\"/>."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: src/filters/about-common-features.xml:0(None)
msgid "translator-credits"
msgstr "島本良太 (SimaMoto,RyōTa) <liangtai.s16@gmail.com>, 2009-2013."

#~ msgid ""
#~ "@@image: 'images/filters/preview-submenu.png'; "
#~ "md5=37842fe59e34e7e605e35434c8505995"
#~ msgstr "localized"

#~ msgid ""
#~ "Most filters have a Preview where changes in the image are displayed, in "
#~ "real time (if the <quote>Preview</quote> option is checked), before being "
#~ "applied to the image."
#~ msgstr ""
#~ "ほとんどのフィルターはプレビュー機能がついていて、 <quote>プレビュー</"
#~ "quote>オプションを有効にしておれば画像に実際にフィルターをかける前からダイ"
#~ "アログ上で調節したとおりに即座に効果の様子が見て判るようになっています。"

#~ msgid "Preview submenu"
#~ msgstr "プレビューのサブメニュー"

#~ msgid ""
#~ "Right clicking on the Preview window opens a submenu which lets you set "
#~ "the Style and the Size of checks representing transparency."
#~ msgstr ""
#~ "プレビューウィンドウ上を <mousebutton>第2ボタン</mousebutton> でクリックす"
#~ "るとメニューが現れ、 透過部分を示す市松模様の色あいや大きさが調節できる。"

#~ msgid "Alpha to Logo Filters"
#~ msgstr "ロゴ効果フィルター"
