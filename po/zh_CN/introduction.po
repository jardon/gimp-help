# Simplified Chinese translation of gimp-help-2-glossary.
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same licenses as the gimp-help-2 package.
# Aron Xu <aronxu@gnome.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gimp-help 2-pot\n"
"POT-Creation-Date: 2023-03-05 19:42+0000\n"
"PO-Revision-Date: 2009-06-23 19:35+0800\n"
"Last-Translator: Aron Xu <aronxu@gnome.org>\n"
"Language-Team: Chinese (simplified)\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/introduction/whats-new.xml:6(title)
msgid "What's new in GIMP"
msgstr ""

#: src/introduction/whats-new.xml:8(para)
msgid ""
"The best way to keep up-to-date with the latest changes in <acronym>GIMP</"
"acronym> is to check the <ulink url=\"https://www.gimp.org/news/\">news</"
"ulink> on our website."
msgstr ""

#: src/introduction/whats-new.xml:13(para)
msgid ""
"We realize that it would be nice to have localized changes of the latest "
"release that links to relevant pages in our documentation. However, our team "
"just doesn't have enough volunteers to make that happen. We would welcome "
"more people helping out with updating or translating our documentation, so "
"don't hesitate to <ulink url=\"https://docs.gimp.org/help.html\">contact us</"
"ulink>."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: src/introduction/whats-new.xml:0(None)
msgid "translator-credits"
msgstr "Aron Xu <aronxu@gnome.org>"

#~ msgid ""
#~ "@@image: 'images/using/empty-image-window.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/empty-image-window.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"

#~ msgid ""
#~ "@@image: 'images/using/scroll-beyond-border.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/scroll-beyond-border.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"

#~ msgid ""
#~ "@@image: 'images/using/new-free-select-tool.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/new-free-select-tool.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"

#~ msgid ""
#~ "@@image: 'images/using/brush-dynamics.jpg'; md5=THIS FILE DOESN'T EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/brush-dynamics.jpg'; md5=THIS FILE DOESN'T EXIST"

#~ msgid "@@image: 'images/using/text-tool.png'; md5=THIS FILE DOESN'T EXIST"
#~ msgstr "@@image: 'images/using/text-tool.png'; md5=THIS FILE DOESN'T EXIST"

#~ msgid ""
#~ "@@image: 'images/using/rectangle-handles.png'; md5=THIS FILE DOESN'T EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/rectangle-handles.png'; md5=THIS FILE DOESN'T EXIST"

#~ msgid ""
#~ "@@image: 'images/using/gimp-curves-tool-2-4-vs-2-6.png'; md5=THIS FILE "
#~ "DOESN'T EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/gimp-curves-tool-2-4-vs-2-6.png'; md5=THIS FILE "
#~ "DOESN'T EXIST"

#~ msgid ""
#~ "@@image: 'images/using/experimental-gegl-tool.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"
#~ msgstr ""
#~ "@@image: 'images/using/experimental-gegl-tool.png'; md5=THIS FILE DOESN'T "
#~ "EXIST"

#~ msgid "User Interface"
#~ msgstr "用户界面"

#~ msgid "GEGL"
#~ msgstr "GEGL"
