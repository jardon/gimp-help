<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect2 PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                       "http://www.docbook.org/xml/4.3/docbookx.dtd">
<!-- section history:
  2017-10-14 j.h: Some updates
-->
<sect2 id="gimp-filter-pixelize" xmlns:xi="http://www.w3.org/2001/XInclude">
  <title>Pixelize</title>

  <indexterm>
    <primary>Filters</primary>
    <secondary>Blur</secondary>
    <tertiary>Pixelize</tertiary>
  </indexterm>
  <indexterm>
    <primary>Pixelize</primary>
  </indexterm>

  <sect3>
    <title>Overview</title>
    <figure>
      <title>Example for the <quote>Pixelize</quote> filter</title>
      <mediaobject>
        <imageobject>
          <imagedata format="JPG"
            fileref="images/filters/examples/taj_orig.jpg"/>
        </imageobject>
        <caption>
          <para>Original</para>
        </caption>
      </mediaobject>
      <mediaobject>
        <imageobject>
          <imagedata format="JPG"
            fileref="images/filters/examples/blur-taj-pixelise.jpg"/>
        </imageobject>
        <caption>
          <para><quote>Pixelize</quote> applied</para>
        </caption>
      </mediaobject>
    </figure>
    <para>
      The Pixelize filter renders the image using large color blocks. It is
      very similar to the effect seen on television when obscuring a
      criminal during trial. It is used for the <quote>Abraham Lincoln
      effect</quote>: see <xref linkend="bibliography-online-bach"/>.
    </para>
  </sect3>

  <sect3>
    <title>Activate the filter</title>
    <para>
      You can find this filter in the image menu through
      <menuchoice>
        <guimenu>Filters</guimenu>
        <guisubmenu>Blur</guisubmenu>
        <guimenuitem>Pixelize…</guimenuitem>
      </menuchoice>
    </para>
  </sect3>

  <sect3>
    <title>Options</title>
    <figure>
      <title><quote>Pixelize</quote> filter options</title>
      <mediaobject>
        <imageobject>
          <imagedata format="PNG"
            fileref="images/filters/blur/pixelize-options.png"/>
        </imageobject>
      </mediaobject>
    </figure>
    <variablelist>
      <varlistentry>
        <term>
          Presets, <quote>Input Type</quote>, Clipping, Blending Options,
          Preview, Split view
        </term>
        <listitem>
          <xi:include href="../about-common-features.xml"/>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Shape</term>
        <listitem>
          <para>
            TODO. Not working
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Block width</term>
        <term>Block height</term>
        <listitem>
          <para>
            Here you can set the desired width and height of the blocks, in
            pixels.
          </para>
          <para>
            By default, width and height are linked, indicated by the chain
            symbol next to the input boxes. If you want to set width and
            height separately, click on that chain symbol to unlink them.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Size ratio X</term>
        <term>Size ratio Y</term>
        <listitem>
          <para>
            Horizontal/Vertical size ratio (0.000-1.000) of a pixel inside a
            block. Default value is 1.000. The number of blocks remains the
            same; so, if you change ratio, block size changes, and missing
            pixels are replaced with the background color.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Background color</term>
        <listitem>
          <para>
            Default background is that of toolbox. You can change it by
            clicking on color source or picking a color using the color picker
            on the right.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
  </sect3>
</sect2>
