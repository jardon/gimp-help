msgid ""
msgstr ""
"Project-Id-Version: gimp-help-keys\n"
"POT-Creation-Date: 2022-03-01 17:30-0500\n"
"PO-Revision-Date: 2010-03-15 00:15+0300\n"
"Last-Translator: Alexandre Prokoudine <alexandre.prokoudine@gmail.com>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gimp-keys.xml:2(title)
msgid "GIMP Quickreference"
msgstr "Быстрая справка GIMP"

#: gimp-keys.xml:5(title)
msgid "File"
msgstr "Файл"

#: gimp-keys.xml:8(key)
msgid "<ctrl/>N"
msgstr "<ctrl/>Н"

#: gimp-keys.xml:9(action)
msgid "New image"
msgstr "Создать изображение"

#: gimp-keys.xml:12(key)
msgid "<ctrl/>O"
msgstr "<ctrl/>О"

#: gimp-keys.xml:13(action)
msgid "Open image"
msgstr "Открыть изображение"

#: gimp-keys.xml:16(key) gimp-keys.xml:125(key)
msgid "<shift/><ctrl/>V"
msgstr "<shift/><ctrl/>V"

#: gimp-keys.xml:17(action)
#, fuzzy
msgid "Create From Clipboard"
msgstr "Вставить буфер обмена"

#: gimp-keys.xml:20(key)
msgid "<ctrl/><alt/>O"
msgstr "<ctrl/><alt/>O"

#: gimp-keys.xml:21(action)
#, fuzzy
msgid "Open image as layers"
msgstr "Открыть изображение как слой"

#: gimp-keys.xml:24(key)
msgid "<ctrl/>1"
msgstr "<ctrl/>1"

#: gimp-keys.xml:24(action)
msgid "Open recent image 01"
msgstr "Открыть последнее изображение 01"

#: gimp-keys.xml:27(key)
msgid "<ctrl/>2"
msgstr "<ctrl/>2"

#: gimp-keys.xml:27(action)
msgid "Open recent image 02"
msgstr "Открыть последнее изображение 02"

#: gimp-keys.xml:30(key)
msgid "<ctrl/>3"
msgstr "<ctrl/>3"

#: gimp-keys.xml:30(action)
msgid "Open recent image 03"
msgstr "Открыть последнее изображение 03"

#: gimp-keys.xml:33(key)
msgid "<ctrl/>4"
msgstr "<ctrl/>4"

#: gimp-keys.xml:33(action)
msgid "Open recent image 04"
msgstr "Открыть последнее изображение 04"

#: gimp-keys.xml:36(key)
msgid "<ctrl/>5"
msgstr "<ctrl/>5"

#: gimp-keys.xml:36(action)
msgid "Open recent image 05"
msgstr "Открыть последнее изображение 05"

#: gimp-keys.xml:39(key)
msgid "<ctrl/>6"
msgstr "<ctrl/>6"

#: gimp-keys.xml:39(action)
msgid "Open recent image 06"
msgstr "Открыть последнее изображение 06"

#: gimp-keys.xml:42(key)
msgid "<ctrl/>7"
msgstr "<ctrl/>7"

#: gimp-keys.xml:42(action)
msgid "Open recent image 07"
msgstr "Открыть последнее изображение 07"

#: gimp-keys.xml:45(key)
msgid "<ctrl/>8"
msgstr "<ctrl/>8"

#: gimp-keys.xml:45(action)
msgid "Open recent image 08"
msgstr "Открыть последнее изображение 08"

#: gimp-keys.xml:48(key)
msgid "<ctrl/>9"
msgstr "<ctrl/>9"

#: gimp-keys.xml:48(action)
msgid "Open recent image 09"
msgstr "Открыть последнее изображение 09"

#: gimp-keys.xml:51(key)
msgid "<ctrl/>0"
msgstr "<ctrl/>0"

#: gimp-keys.xml:51(action)
msgid "Open recent image 10"
msgstr "Открыть последнее изображение 10"

#: gimp-keys.xml:54(key)
msgid "<ctrl/>S"
msgstr "<ctrl/>S"

#: gimp-keys.xml:54(action)
#, fuzzy
msgid "Save the XCF image"
msgstr "Сохранить изображение"

#: gimp-keys.xml:57(key)
msgid "<shift/><ctrl/>S"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:58(action)
msgid "Save as... : Save image with a different name"
msgstr ""

#: gimp-keys.xml:61(key)
msgid "<ctrl/>E"
msgstr "<ctrl/>E"

#: gimp-keys.xml:61(action)
msgid "Export"
msgstr ""

#: gimp-keys.xml:64(key)
msgid "<shift/><ctrl/>E"
msgstr "<shift/><ctrl/>E"

#: gimp-keys.xml:65(action)
msgid "Export As...: export image to various file formats"
msgstr ""

#: gimp-keys.xml:68(key)
msgid "<ctrl/>P"
msgstr "<ctrl/>P"

#: gimp-keys.xml:68(action)
msgid "Print..."
msgstr ""

#: gimp-keys.xml:71(key)
#, fuzzy
msgid "<ctrl/><alt/>F"
msgstr "<ctrl/><alt/>O"

#: gimp-keys.xml:72(action)
msgid "Show the image in file manager"
msgstr ""

#: gimp-keys.xml:75(key) gimp-keys.xml:195(key)
msgid "<ctrl/>W"
msgstr "<ctrl/>W"

#: gimp-keys.xml:75(action)
#, fuzzy
msgid "Close Window"
msgstr "Закрыть окно"

#: gimp-keys.xml:78(key)
#, fuzzy
msgid "<shift/><ctrl/>W"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:78(action)
#, fuzzy
msgid "Close All"
msgstr "Закрыть диалог"

#: gimp-keys.xml:81(key)
msgid "<ctrl/>Q"
msgstr "<ctrl/>Q"

#: gimp-keys.xml:81(action)
msgid "Quit"
msgstr "Завершить работу с программой"

#: gimp-keys.xml:87(title)
msgid "Edit"
msgstr "Правка"

#: gimp-keys.xml:89(title)
msgid "Undo/redo"
msgstr "Отменить/вернуть"

#: gimp-keys.xml:91(key)
msgid "<ctrl/>Z"
msgstr "<ctrl/>Z"

#: gimp-keys.xml:91(action)
msgid "Undo"
msgstr "Отменить"

#: gimp-keys.xml:94(key)
msgid "<ctrl/>Y"
msgstr "<ctrl/>Y"

#: gimp-keys.xml:94(action)
msgid "Redo"
msgstr "Вернуть"

#: gimp-keys.xml:99(title)
msgid "Clipboard"
msgstr "Буфер обмена"

#: gimp-keys.xml:101(key)
msgid "<ctrl/>C"
msgstr "<ctrl/>C"

#: gimp-keys.xml:101(action)
msgid "Copy selection"
msgstr "Скопировать выделение"

#: gimp-keys.xml:103(note)
msgid "This places a copy of the selection to the GIMP clipboard."
msgstr "Помещает копию выделения в буфер обмена GIMP."

#: gimp-keys.xml:107(key)
msgid "<shift/><ctrl/>C"
msgstr "<shift/><ctrl/>C"

#: gimp-keys.xml:107(action)
#, fuzzy
msgid "Copy visible"
msgstr "Скопировать выделение"

#: gimp-keys.xml:110(key)
msgid "<ctrl/>X"
msgstr "<ctrl/>X"

#: gimp-keys.xml:110(action)
msgid "Cut selection"
msgstr "Вырезать выделение"

#: gimp-keys.xml:112(note)
#, fuzzy
msgid ""
"This works the same as “Copy” the selection followed by “Clear” the "
"selection."
msgstr "Сначала копирует выделение в буфер обмена, затем удаляет выделение."

#: gimp-keys.xml:116(key)
msgid "<ctrl/>V"
msgstr "<ctrl/>V"

#: gimp-keys.xml:116(action)
msgid "Paste clipboard"
msgstr "Вставить буфер обмена"

#: gimp-keys.xml:118(note)
msgid "This places the clipboard objects as a floating selection"
msgstr "Помещает объекты в буфере обмена как плавающее выделение"

#: gimp-keys.xml:122(key)
#, fuzzy
msgid "<ctrl/><alt/>V"
msgstr "<ctrl/><alt/>O"

#: gimp-keys.xml:122(action)
#, fuzzy
msgid "Paste in place"
msgstr "Вставить буфер обмена"

#: gimp-keys.xml:125(action)
msgid "Paste as new image"
msgstr ""

#: gimp-keys.xml:130(title)
msgid "Fill"
msgstr "Заливка"

#: gimp-keys.xml:132(action)
msgid "Clear"
msgstr ""

#: gimp-keys.xml:135(key)
#, fuzzy
msgid "<ctrl/>,"
msgstr "<ctrl/>Н"

#: gimp-keys.xml:135(action)
msgid "Fill with FG Color"
msgstr "Заливка цветом переднего плана"

#: gimp-keys.xml:138(key)
#, fuzzy
msgid "<ctrl/>."
msgstr "<ctrl/>Н"

#: gimp-keys.xml:138(action)
msgid "Fill with BG Color"
msgstr "Заливка цветом фона"

#: gimp-keys.xml:141(key)
#, fuzzy
msgid "<ctrl/>;"
msgstr "<ctrl/>Н"

#: gimp-keys.xml:141(action)
msgid "Fill with Pattern"
msgstr "Заливка текстурой"

#: gimp-keys.xml:147(title)
#, fuzzy
msgid "Select"
msgstr "Выделения"

#: gimp-keys.xml:150(key)
msgid "<ctrl/>T"
msgstr "<ctrl/>T"

#: gimp-keys.xml:150(action)
msgid "Toggle selections"
msgstr "Переключение выделений"

#: gimp-keys.xml:153(key)
msgid "<ctrl/>A"
msgstr "<ctrl/>A"

#: gimp-keys.xml:153(action)
msgid "Select all"
msgstr "Выделить всё"

#: gimp-keys.xml:156(key)
msgid "<shift/><ctrl/>A"
msgstr "<shift/><ctrl/>A"

#: gimp-keys.xml:156(action)
msgid "Select none"
msgstr "Выделить ничего"

#: gimp-keys.xml:159(key)
msgid "<ctrl/>I"
msgstr "<ctrl/>I"

#: gimp-keys.xml:159(action)
msgid "Invert selection"
msgstr "Инвертировать выделение"

#: gimp-keys.xml:162(key)
msgid "<shift/><ctrl/>L"
msgstr "<shift/><ctrl/>L"

#: gimp-keys.xml:162(action)
msgid "Float selection"
msgstr "Сделать выделение плавающим"

#: gimp-keys.xml:165(key)
msgid "<shift/>V"
msgstr "<shift/>V"

#: gimp-keys.xml:165(action)
msgid "Path to selection"
msgstr "Контур в выделение"

#: gimp-keys.xml:171(title)
msgid "View"
msgstr "Вид"

#: gimp-keys.xml:173(title)
msgid "Window"
msgstr "Окно"

#: gimp-keys.xml:174(note)
msgid ""
"Menus can also be activated by Alt with the letter underscored in the menu "
"name."
msgstr ""
"Пункты меню также активируются клавишей Alt с подчёркнутой буквой в имени "
"пункта."

#: gimp-keys.xml:178(action)
msgid "Main Menu"
msgstr "Главное меню"

#: gimp-keys.xml:182(action)
msgid "Drop-down Menu"
msgstr "Раскрывающееся меню"

#: gimp-keys.xml:185(action)
msgid "Toggle fullscreen"
msgstr "Переключение в полноэкранный режим"

#: gimp-keys.xml:189(action)
msgid "Toggle the visibility of toolbox and dialogs docks"
msgstr ""

#: gimp-keys.xml:192(key)
msgid "<shift/>Q"
msgstr "<shift/>П"

#: gimp-keys.xml:192(action)
msgid "Toggle quickmask"
msgstr "Переключить быструю маску"

#: gimp-keys.xml:195(action)
msgid "Close document window"
msgstr "Закрыть окно документа"

#: gimp-keys.xml:198(key)
#, fuzzy
msgid "<shift/>J"
msgstr "<shift/>O"

#: gimp-keys.xml:198(action)
#, fuzzy
msgid "Center image in window"
msgstr "Помещает изображение в окне"

#: gimp-keys.xml:201(key)
#, fuzzy
msgid "<shift/><ctrl/>J"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:201(action)
msgid "Fit image in window"
msgstr "Помещает изображение в окне"

#: gimp-keys.xml:206(title)
#: gimp-keys.xml:226(action)
#: gimp-keys.xml:463(action)
msgid "Zoom"
msgstr "Масштаб"

#: gimp-keys.xml:208(key)
msgid "+"
msgstr "+"

#: gimp-keys.xml:208(action)
#, fuzzy
msgid "Zoom In"
msgstr "Приблизить"

#: gimp-keys.xml:211(key)
msgid "-"
msgstr "-"

#: gimp-keys.xml:211(action)
#, fuzzy
msgid "Zoom Out"
msgstr "Отдалить"

#: gimp-keys.xml:214(key)
msgid "1"
msgstr "1"

#: gimp-keys.xml:214(action)
msgid "Zoom 1:1"
msgstr "Масштаб 1:1"

#: gimp-keys.xml:217(key)
msgid "`"
msgstr ""

#: gimp-keys.xml:217(action)
msgid "Revert zoom"
msgstr ""

#: gimp-keys.xml:220(key)
#, fuzzy
msgid "<ctrl/>J"
msgstr "<ctrl/>Н"

#: gimp-keys.xml:220(action)
msgid "Shrink wrap"
msgstr "Сократить окно по изображению"

#: gimp-keys.xml:222(note)
#, fuzzy
msgid "This fits the window to the image size."
msgstr "Уменьшает окно по размеру изображения"

#: gimp-keys.xml:231(title)
msgid "Flip and Rotate (0°)"
msgstr ""

#: gimp-keys.xml:233(key)
msgid "!"
msgstr ""

#: gimp-keys.xml:233(action)
msgid "Reset Flip and Rotate"
msgstr ""

#: gimp-keys.xml:238(title)
msgid "Scrolling (panning)"
msgstr "Прокрутка"

#: gimp-keys.xml:240(action)
#: gimp-keys.xml:246(action)
msgid "Scroll canvas"
msgstr "Прокрутка холста"

#: gimp-keys.xml:242(note)
#, fuzzy
msgid ""
"Scrolling by keys is accelerated, i.e. it speeds up when you press Shift"
"+arrows."
msgstr ""
"Прокрутка с помощью клавиш ускорена, т.е. увеличивается при нажатии Shift"
"+стрелки, или идёт к краям при нажатии Ctrl+стрелки."

#: gimp-keys.xml:249(action)
msgid "Scroll canvas vertically"
msgstr "Прокрутить холст вертикально "

#: gimp-keys.xml:252(action)
msgid "Scroll canvas horizontally"
msgstr "Прокрутить холст горизонтально"

#: gimp-keys.xml:257(title)
msgid "Rulers and Guides"
msgstr "Линейки и направляющие"

#: gimp-keys.xml:259(action)
msgid "Drag off a ruler to create guide"
msgstr "Выполнение этой комбинации над линейкой создаёт новую направляющую"

#: gimp-keys.xml:261(note)
#, fuzzy
msgid ""
"Drag off the horizontal or vertical ruler to create a new guide line. Drag a "
"guide line onto the ruler to delete it."
msgstr ""
"Сдвиньте с горизонтальной или вертикальной линейки, чтобы создать новую "
"направляющую"

#: gimp-keys.xml:265(action)
msgid "Drag a sample point out of the rulers"
msgstr "Выполнение этой комбинации над линейкой создаёт новую образцовую точку"

#: gimp-keys.xml:268(key)
msgid "<shift/><ctrl/>R"
msgstr "<shift/><ctrl/>R"

#: gimp-keys.xml:268(action)
msgid "Toggle rulers"
msgstr "Переключение линеек"

#: gimp-keys.xml:271(key)
msgid "<shift/><ctrl/>T"
msgstr "<shift/><ctrl/>T"

#: gimp-keys.xml:271(action)
msgid "Toggle guides"
msgstr "Переключение направляющих"

#: gimp-keys.xml:279(title)
msgid "Image"
msgstr ""

#: gimp-keys.xml:282(key)
msgid "<ctrl/>D"
msgstr "<ctrl/>D"

#: gimp-keys.xml:282(action)
msgid "Duplicate image"
msgstr "Создать копию изображения"

#: gimp-keys.xml:286(action)
msgid "Image properties"
msgstr ""

#: gimp-keys.xml:292(title)
#: gimp-keys.xml:507(action)
msgid "Layers"
msgstr "Слои"

#: gimp-keys.xml:295(key)
msgid "<shift/><ctrl/>N"
msgstr "<shift/><ctrl/>N"

#: gimp-keys.xml:295(action)
#, fuzzy
msgid "New layer"
msgstr "Создать изображение"

#: gimp-keys.xml:298(key)
#, fuzzy
msgid "<shift/><ctrl/>D"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:298(action)
#, fuzzy
msgid "Duplicate layer"
msgstr "Создать копию изображения"

#: gimp-keys.xml:302(action)
msgid "Select the layer above"
msgstr "Выделить слой выше"

#: gimp-keys.xml:306(action)
msgid "Select the layer below"
msgstr "Выделить слой ниже"

#: gimp-keys.xml:309(key)
msgid "<ctrl/>M"
msgstr "<ctrl/>M"

#: gimp-keys.xml:309(action)
msgid "Merge visible layers"
msgstr "Объединить видимые слои"

#: gimp-keys.xml:312(key)
msgid "<ctrl/>H"
msgstr "<ctrl/>H"

#: gimp-keys.xml:312(action)
#, fuzzy
msgid "Anchor layer"
msgstr "Прицепить слой"

#: gimp-keys.xml:318(title)
#: gimp-keys.xml:474(action)
msgid "Toolbox"
msgstr "Инструменты"

#: gimp-keys.xml:320(title)
msgid "Tools"
msgstr "Инструменты"

#: gimp-keys.xml:322(key)
msgid "R"
msgstr "Я"

#: gimp-keys.xml:323(action)
#, fuzzy
msgid "Rectangle Select"
msgstr "Прямоугольное выделение"

#: gimp-keys.xml:326(key)
msgid "E"
msgstr "Э"

#: gimp-keys.xml:327(action)
msgid "Ellipse Select"
msgstr "Эллиптическое выделение"

#: gimp-keys.xml:330(key)
msgid "F"
msgstr "В"

#: gimp-keys.xml:331(action)
msgid "Free Select"
msgstr "Свободное выделение"

#: gimp-keys.xml:334(key) gimp-keys.xml:462(key)
msgid "Z"
msgstr "Ы"

#: gimp-keys.xml:335(action)
msgid "Fuzzy Select"
msgstr "Выделение связанной области"

#: gimp-keys.xml:338(key)
msgid "<shift/>O"
msgstr "<shift/>O"

#: gimp-keys.xml:339(action)
msgid "Select By Color"
msgstr "Выделение по цвету"

#: gimp-keys.xml:342(key)
msgid "I"
msgstr "I"

#: gimp-keys.xml:343(action)
msgid "Intelligent Scissors"
msgstr ""

#: gimp-keys.xml:346(key)
msgid "<shift/>B"
msgstr "<shift/>З"

#: gimp-keys.xml:347(action)
msgid "Bucket Fill"
msgstr "Плоская заливка"

#: gimp-keys.xml:350(key)
msgid "G"
msgstr ""

#: gimp-keys.xml:351(action)
#, fuzzy
msgid "Gradient"
msgstr "Градиенты"

#: gimp-keys.xml:354(key)
msgid "N"
msgstr "К"

#: gimp-keys.xml:355(action)
msgid "Pencil"
msgstr "Карандаш"

#: gimp-keys.xml:358(key)
msgid "P"
msgstr "К"

#: gimp-keys.xml:359(action)
msgid "Paintbrush"
msgstr "Кисть"

#: gimp-keys.xml:362(key)
msgid "<shift/>E"
msgstr "<shift/>Л"

#: gimp-keys.xml:363(action)
msgid "Eraser"
msgstr "Ластик"

#: gimp-keys.xml:366(key)
msgid "A"
msgstr "А"

#: gimp-keys.xml:367(action)
msgid "Airbrush"
msgstr "Аэрограф"

#: gimp-keys.xml:370(key)
msgid "K"
msgstr "Е"

#: gimp-keys.xml:371(action)
msgid "Ink"
msgstr "Перо"

#: gimp-keys.xml:374(key)
msgid "Y"
msgstr ""

#: gimp-keys.xml:375(action)
#, fuzzy
msgid "MyPaint Brush"
msgstr "Кисть"

#: gimp-keys.xml:378(key)
msgid "C"
msgstr "Ш"

#: gimp-keys.xml:379(action)
msgid "Clone"
msgstr "Штамп"

#: gimp-keys.xml:382(key)
msgid "H"
msgstr ""

#: gimp-keys.xml:383(action)
msgid "Heal"
msgstr ""

#: gimp-keys.xml:386(key)
msgid "<shift/>U"
msgstr "<shift/>U"

#: gimp-keys.xml:387(action)
msgid "Blur/Sharpen"
msgstr "Blur/Sharpen"

#: gimp-keys.xml:390(key)
msgid "S"
msgstr "Ц"

#: gimp-keys.xml:391(action)
msgid "Smudge"
msgstr "Палец"

#: gimp-keys.xml:394(key)
msgid "<shift/>D"
msgstr "<shift/>D"

#: gimp-keys.xml:395(action)
msgid "Dodge/Burn"
msgstr "Осветление/Затемнение"

#: gimp-keys.xml:398(key)
msgid "Q"
msgstr ""

#: gimp-keys.xml:399(action)
msgid "Alignment"
msgstr ""

#: gimp-keys.xml:402(key)
msgid "M"
msgstr "П"

#: gimp-keys.xml:403(action)
msgid "Move"
msgstr "Перемещение"

#: gimp-keys.xml:406(key)
msgid "<shift/>C"
msgstr "<shift/>C"

#: gimp-keys.xml:407(action)
msgid "Crop"
msgstr ""

#: gimp-keys.xml:410(key)
msgid "<shift/>R"
msgstr "<shift/>В"

#: gimp-keys.xml:411(action)
msgid "Rotate"
msgstr "Вращение"

#: gimp-keys.xml:414(key)
msgid "<shift/>S"
msgstr "<shift/>S"

#: gimp-keys.xml:415(action)
msgid "Scale"
msgstr "Масштаб"

#: gimp-keys.xml:418(key)
#, fuzzy
msgid "<shift/>H"
msgstr "<shift/>O"

#: gimp-keys.xml:419(action)
msgid "Shear"
msgstr "Искривление"

#: gimp-keys.xml:422(key)
msgid "<shift/>P"
msgstr "<shift/>P"

#: gimp-keys.xml:423(action)
msgid "Perspective"
msgstr "Перспектива"

#: gimp-keys.xml:426(key)
msgid "<shift/>T"
msgstr "<shift/>T"

#: gimp-keys.xml:427(action)
msgid "Unified Transform"
msgstr ""

#: gimp-keys.xml:430(key)
msgid "<shift/>L"
msgstr "<shift/>Е"

#: gimp-keys.xml:431(action)
msgid "Handle Transform"
msgstr ""

#: gimp-keys.xml:434(key)
msgid "<shift/>F"
msgstr "<shift/>F"

#: gimp-keys.xml:435(action)
msgid "Flip"
msgstr "Зеркало"

#: gimp-keys.xml:438(key)
#, fuzzy
msgid "<shift/>G"
msgstr "<shift/>O"

#: gimp-keys.xml:439(action)
msgid "Cage Transform"
msgstr ""

#: gimp-keys.xml:442(key)
msgid "W"
msgstr ""

#: gimp-keys.xml:443(action)
msgid "Warp Transform"
msgstr ""

#: gimp-keys.xml:446(key)
msgid "B"
msgstr "B"

#: gimp-keys.xml:447(action)
msgid "Paths"
msgstr "Контуры"

#: gimp-keys.xml:450(key)
msgid "T"
msgstr "Т"

#: gimp-keys.xml:451(action)
msgid "Text"
msgstr "Текст"

#: gimp-keys.xml:454(key)
msgid "O"
msgstr "П"

#: gimp-keys.xml:455(action)
msgid "Color Picker"
msgstr "Пипетка"

#: gimp-keys.xml:458(key)
#, fuzzy
msgid "<shift/>M"
msgstr "<shift/>O"

#: gimp-keys.xml:459(action)
msgid "Measure"
msgstr ""

#: gimp-keys.xml:465(note)
msgid "Double click on the tool buttons opens the Tool Options dialog."
msgstr ""
"Двойной щелчок по кнопке инструмента открывает диалог с его параметрами"

#: gimp-keys.xml:471(title)
msgid "Context"
msgstr "Контекст"

#: gimp-keys.xml:473(key)
#, fuzzy
msgid "<Ctrl/>B"
msgstr "<ctrl/>Н"

#: gimp-keys.xml:477(key)
msgid "D"
msgstr "Ц"

#: gimp-keys.xml:478(action)
msgid "Default Colors"
msgstr "Цвета по умолчанию"

#: gimp-keys.xml:481(key)
msgid "X"
msgstr "X"

#: gimp-keys.xml:482(action)
msgid "Swap Colors"
msgstr "Поменять местами цвета"

#: gimp-keys.xml:484(note)
msgid "Click on the colors to change the colors."
msgstr "Щёлкните по цветам, чтобы изменить их"

#: gimp-keys.xml:492(title)
msgid "Filters"
msgstr ""

#: gimp-keys.xml:495(key)
msgid "<ctrl/>F"
msgstr "<ctrl/>F"

#: gimp-keys.xml:495(action)
#, fuzzy
msgid "Repeat last filter"
msgstr "Повторить последнее расширение"

#: gimp-keys.xml:498(key)
msgid "<shift/><ctrl/>F"
msgstr "<shift/><ctrl/>F"

#: gimp-keys.xml:498(action)
#, fuzzy
msgid "Reshow last filter"
msgstr "Показать диалог последнего расширения"

#: gimp-keys.xml:504(title)
#, fuzzy
msgid "Windows"
msgstr "Окно"

#: gimp-keys.xml:507(key)
msgid "<ctrl/>L"
msgstr "<ctrl/>L"

#: gimp-keys.xml:510(key)
msgid "<shift/><ctrl/>B"
msgstr "<shift/><ctrl/>B"

#: gimp-keys.xml:510(action)
msgid "Brushes"
msgstr "Кисти"

#: gimp-keys.xml:513(key)
msgid "<shift/><ctrl/>P"
msgstr "<shift/><ctrl/>P"

#: gimp-keys.xml:513(action)
msgid "Patterns"
msgstr "Текстуры"

#: gimp-keys.xml:516(key)
msgid "<ctrl/>G"
msgstr "<ctrl/>G"

#: gimp-keys.xml:516(action)
msgid "Gradients"
msgstr "Градиенты"

#: gimp-keys.xml:518(note)
msgid ""
"These open a new dialog window if it wasn't open yet, otherwise the "
"corresponding dialog gets focus."
msgstr ""
"Эти команды отрывают новые окна, если они не были открыты, иначе "
"соответствующему диалогу придаётся фокус."

#: gimp-keys.xml:524(title)
msgid "Within a Dialog"
msgstr "Внутри диалога"

#: gimp-keys.xml:526(action)
msgid "Set the new value"
msgstr "Установить новое значение"

#: gimp-keys.xml:528(note)
msgid ""
"This accepts the new value you typed in a text field and returns focus to "
"canvas."
msgstr "Принимает введённое значение и возвращает фокус холсту."

#: gimp-keys.xml:533(action)
msgid "Activate current button or list"
msgstr "Активировать текущую кнопку или список"

#: gimp-keys.xml:537(title)
#, fuzzy
msgid "Within a multi-tab dialog"
msgstr "Внутри диалога Файла"

#: gimp-keys.xml:540(action)
msgid "Switch tabs up"
msgstr ""

#: gimp-keys.xml:544(action)
msgid "Switch tabs down"
msgstr ""

#: gimp-keys.xml:549(title)
msgid "Within a File Dialog"
msgstr "Внутри диалога Файла"

#: gimp-keys.xml:551(action)
msgid "Up-Folder"
msgstr "Верхняя папка"

#: gimp-keys.xml:554(action)
msgid "Down-Folder"
msgstr "Нижняя папка"

#: gimp-keys.xml:557(action)
msgid "Home-Folder"
msgstr "Домашняя папка"

#: gimp-keys.xml:560(action)
msgid "Close Dialog"
msgstr "Закрыть диалог"

#: gimp-keys.xml:565(title)
#: gimp-keys.xml:569(action)
msgid "Help"
msgstr "Справка"

#: gimp-keys.xml:573(action)
msgid "Context Help"
msgstr "Контекстная помощь"

#: gimp-keys.xml:576(key)
msgid "/"
msgstr ""

#: gimp-keys.xml:577(action)
msgid "Search and run a command"
msgstr ""

#: gimp-keys.xml:585(title)
msgid "Zoom tool"
msgstr "Лупа"

#: gimp-keys.xml:588(action)
msgid "Zoom in"
msgstr "Приблизить"

#: gimp-keys.xml:591(action)
msgid "Zoom out"
msgstr "Отдалить"

#: gimp-keys.xml:595(action)
#, fuzzy
msgid "Zoom in inside the area"
msgstr "Увеличение масштаба области"

#: gimp-keys.xml:599(action)
#, fuzzy
msgid "Zoom out inside the area"
msgstr "Увеличение масштаба области"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: gimp-keys.xml:0(None)
msgid "translator-credits"
msgstr ""
"Виталий Ломов <lomovv@gmail.com>, 2007\n"
"Александр Прокудин <alexandre.prokoudine@gmail.com>, 2010"

#~ msgid "Scissors"
#~ msgstr "Ножницы"

#~ msgid "Crop and Resize"
#~ msgstr "Откадрировать и изменить размер"

#~ msgid "L"
#~ msgstr "L"

#~ msgid "Blend"
#~ msgstr "Градиентная заливка"

#~ msgid "Save under a new name"
#~ msgstr "Сохранить под новым именем"

#~ msgid "Dialogs"
#~ msgstr "Диалоги"

#~ msgid "Tool-Options"
#~ msgstr "Параметры инструментов"

#~ msgid "Palettes"
#~ msgstr "Палитры"

#~ msgid "<shift/><ctrl/>I"
#~ msgstr "<shift/><ctrl/>I"

#~ msgid "Info window"
#~ msgstr "Информационное окно"

#~ msgid "Navigation window"
#~ msgstr "Окно навигации"

#~ msgid "Jump to next widget"
#~ msgstr "Перейти к следующему объекту"

#~ msgid "Jump to previous widget"
#~ msgstr "Перейти к предыдущему объекту"

#~ msgid "In a multi-tab dialog, switch tabs"
#~ msgstr "В диалоге с закладками меняет закладки"

#~ msgid "Open Location"
#~ msgstr "Открыть из сети"

#~ msgid "<ctrl/>K"
#~ msgstr "<ctrl/>K"

#~ msgid "Clears selection"
#~ msgstr "Очищает выделение"

#~ msgid "Named copy selection"
#~ msgstr "Скопировать именованое выделение"

#~ msgid "<shift/><ctrl/>X"
#~ msgstr "<shift/><ctrl/>X"

#~ msgid "Named cut selection"
#~ msgstr "Вырезать именованое выделение"

#~ msgid "Named paste clipboard"
#~ msgstr "Вставить именованый буфер обмена"

#~ msgid "Select the first layer"
#~ msgstr "Выделить первый слой"

#~ msgid "Select the last layer"
#~ msgstr "Выделить последний слой"

#~ msgid "Plug-ins"
#~ msgstr "Расширения"

#~ msgid "V"
#~ msgstr "Р"

#~ msgid "Convolve"
#~ msgstr "Размывание"
