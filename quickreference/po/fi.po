# Finnish translation for gimp-help
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the gimp-help package.
# Rosetta contributors:
# Elias Julkunen <elias.julkunen@gmail.com>, 2008.
# Timo Jyrinki <timo.jyrinki@iki.fi>, 2008.
# Janne Peltonen <peltzi@gimp-suomi.org>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: gimp-help\n"
"Report-Msgid-Bugs-To: gimp\n"
"POT-Creation-Date: 2022-03-01 17:30-0500\n"
"PO-Revision-Date: 2012-07-02 12:38+0300\n"
"Last-Translator: Janne Peltonen <peltzi@gimp-suomi.org>\n"
"Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gimp-keys.xml:2(title)
msgid "GIMP Quickreference"
msgstr "GIMP-pikaopas"

#: gimp-keys.xml:5(title)
msgid "File"
msgstr "Tiedosto"

#: gimp-keys.xml:8(key)
msgid "<ctrl/>N"
msgstr "<ctrl/>N"

#: gimp-keys.xml:9(action)
msgid "New image"
msgstr "Uusi kuva"

#: gimp-keys.xml:12(key)
msgid "<ctrl/>O"
msgstr "<ctrl/>O"

#: gimp-keys.xml:13(action)
msgid "Open image"
msgstr "Avaa kuva"

#: gimp-keys.xml:16(key) gimp-keys.xml:125(key)
msgid "<shift/><ctrl/>V"
msgstr "<shift/><ctrl/>V"

#: gimp-keys.xml:17(action)
#, fuzzy
msgid "Create From Clipboard"
msgstr "Liitä leikepöydältä"

#: gimp-keys.xml:20(key)
msgid "<ctrl/><alt/>O"
msgstr "<ctrl/><alt/>O"

#: gimp-keys.xml:21(action)
#, fuzzy
msgid "Open image as layers"
msgstr "Avaa kuva uutena tasona"

#: gimp-keys.xml:24(key)
msgid "<ctrl/>1"
msgstr "<ctrl/>1"

#: gimp-keys.xml:24(action)
msgid "Open recent image 01"
msgstr "Avaa viimeisin kuva 01"

#: gimp-keys.xml:27(key)
msgid "<ctrl/>2"
msgstr "<ctrl/>2"

#: gimp-keys.xml:27(action)
msgid "Open recent image 02"
msgstr "Avaa viimeisin kuva 02"

#: gimp-keys.xml:30(key)
msgid "<ctrl/>3"
msgstr "<ctrl/>3"

#: gimp-keys.xml:30(action)
msgid "Open recent image 03"
msgstr "Avaa viimeisin kuva 03"

#: gimp-keys.xml:33(key)
msgid "<ctrl/>4"
msgstr "<ctrl/>4"

#: gimp-keys.xml:33(action)
msgid "Open recent image 04"
msgstr "Avaa viimeisin kuva 04"

#: gimp-keys.xml:36(key)
msgid "<ctrl/>5"
msgstr "<ctrl/>5"

#: gimp-keys.xml:36(action)
msgid "Open recent image 05"
msgstr "Avaa viimeisin kuva 05"

#: gimp-keys.xml:39(key)
msgid "<ctrl/>6"
msgstr "<ctrl/>6"

#: gimp-keys.xml:39(action)
msgid "Open recent image 06"
msgstr "Avaa viimeisin kuva 06"

#: gimp-keys.xml:42(key)
msgid "<ctrl/>7"
msgstr "<ctrl/>7"

#: gimp-keys.xml:42(action)
msgid "Open recent image 07"
msgstr "Avaa viimeisin kuva 07"

#: gimp-keys.xml:45(key)
msgid "<ctrl/>8"
msgstr "<ctrl/>8"

#: gimp-keys.xml:45(action)
msgid "Open recent image 08"
msgstr "Avaa viimeisin kuva 08"

#: gimp-keys.xml:48(key)
msgid "<ctrl/>9"
msgstr "<ctrl/>9"

#: gimp-keys.xml:48(action)
msgid "Open recent image 09"
msgstr "Avaa viimeisin kuva 09"

#: gimp-keys.xml:51(key)
msgid "<ctrl/>0"
msgstr "<ctrl/>0"

#: gimp-keys.xml:51(action)
msgid "Open recent image 10"
msgstr "Avaa viimeisin kuva 10"

#: gimp-keys.xml:54(key)
msgid "<ctrl/>S"
msgstr "<ctrl/>S"

#: gimp-keys.xml:54(action)
#, fuzzy
msgid "Save the XCF image"
msgstr "Tallenna kuva"

#: gimp-keys.xml:57(key)
msgid "<shift/><ctrl/>S"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:58(action)
msgid "Save as... : Save image with a different name"
msgstr ""

#: gimp-keys.xml:61(key)
msgid "<ctrl/>E"
msgstr "<ctrl/>E"

#: gimp-keys.xml:61(action)
#, fuzzy
msgid "Export"
msgstr "Vie kohteeseen"

#: gimp-keys.xml:64(key)
msgid "<shift/><ctrl/>E"
msgstr "<shift/><ctrl/>E"

#: gimp-keys.xml:65(action)
msgid "Export As...: export image to various file formats"
msgstr ""

#: gimp-keys.xml:68(key)
msgid "<ctrl/>P"
msgstr "<ctrl/>P"

#: gimp-keys.xml:68(action)
msgid "Print..."
msgstr ""

#: gimp-keys.xml:71(key)
#, fuzzy
msgid "<ctrl/><alt/>F"
msgstr "<ctrl/><alt/>O"

#: gimp-keys.xml:72(action)
msgid "Show the image in file manager"
msgstr ""

#: gimp-keys.xml:75(key) gimp-keys.xml:195(key)
msgid "<ctrl/>W"
msgstr "<ctrl/>W"

#: gimp-keys.xml:75(action)
#, fuzzy
msgid "Close Window"
msgstr "Sulje ikkuna"

#: gimp-keys.xml:78(key)
#, fuzzy
msgid "<shift/><ctrl/>W"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:78(action)
#, fuzzy
msgid "Close All"
msgstr "Sulje valintaikkuna"

#: gimp-keys.xml:81(key)
msgid "<ctrl/>Q"
msgstr "<ctrl/>Q"

#: gimp-keys.xml:81(action)
msgid "Quit"
msgstr "Poistu"

#: gimp-keys.xml:87(title)
msgid "Edit"
msgstr "Muokkaa"

#: gimp-keys.xml:89(title)
msgid "Undo/redo"
msgstr "Kumoa/tee uudelleen"

#: gimp-keys.xml:91(key)
msgid "<ctrl/>Z"
msgstr "<ctrl/>Z"

#: gimp-keys.xml:91(action)
msgid "Undo"
msgstr "Kumoa"

#: gimp-keys.xml:94(key)
msgid "<ctrl/>Y"
msgstr "<ctrl/>Y"

#: gimp-keys.xml:94(action)
msgid "Redo"
msgstr "Tee uudelleen"

#: gimp-keys.xml:99(title)
msgid "Clipboard"
msgstr "Leikepöytä"

#: gimp-keys.xml:101(key)
msgid "<ctrl/>C"
msgstr "<ctrl/>C"

#: gimp-keys.xml:101(action)
msgid "Copy selection"
msgstr "Kopioi valinta"

#: gimp-keys.xml:103(note)
msgid "This places a copy of the selection to the GIMP clipboard."
msgstr "Asettaa kopion valinnasta GIMP-leikepöydälle"

#: gimp-keys.xml:107(key)
msgid "<shift/><ctrl/>C"
msgstr "<shift/><ctrl/>C"

#: gimp-keys.xml:107(action)
#, fuzzy
msgid "Copy visible"
msgstr "Kopioi valinta"

#: gimp-keys.xml:110(key)
msgid "<ctrl/>X"
msgstr "<ctrl/>X"

#: gimp-keys.xml:110(action)
msgid "Cut selection"
msgstr "Leikkaa valinta"

#: gimp-keys.xml:112(note)
#, fuzzy
msgid ""
"This works the same as “Copy” the selection followed by “Clear” the "
"selection."
msgstr ""
"Tämä toimii samalla tavalla kuin \"kopioi valinta\" jota seuraa valinnan "
"poisto."

#: gimp-keys.xml:116(key)
msgid "<ctrl/>V"
msgstr "<ctrl/>V"

#: gimp-keys.xml:116(action)
msgid "Paste clipboard"
msgstr "Liitä leikepöydältä"

#: gimp-keys.xml:118(note)
msgid "This places the clipboard objects as a floating selection"
msgstr "Sijoittaa leikepöydän objektit leijuvana valintana"

#: gimp-keys.xml:122(key)
#, fuzzy
msgid "<ctrl/><alt/>V"
msgstr "<ctrl/><alt/>O"

#: gimp-keys.xml:122(action)
#, fuzzy
msgid "Paste in place"
msgstr "Liitä leikepöydältä"

#: gimp-keys.xml:125(action)
msgid "Paste as new image"
msgstr ""

#: gimp-keys.xml:130(title)
msgid "Fill"
msgstr "Täytä"

#: gimp-keys.xml:132(action)
msgid "Clear"
msgstr ""

#: gimp-keys.xml:135(key)
#, fuzzy
msgid "<ctrl/>,"
msgstr "<ctrl/>N"

#: gimp-keys.xml:135(action)
msgid "Fill with FG Color"
msgstr "Täytä edustavärillä"

#: gimp-keys.xml:138(key)
#, fuzzy
msgid "<ctrl/>."
msgstr "<ctrl/>N"

#: gimp-keys.xml:138(action)
msgid "Fill with BG Color"
msgstr "Täytä taustavärillä"

#: gimp-keys.xml:141(key)
#, fuzzy
msgid "<ctrl/>;"
msgstr "<ctrl/>N"

#: gimp-keys.xml:141(action)
msgid "Fill with Pattern"
msgstr "Täytä kuviolla"

#: gimp-keys.xml:147(title)
#, fuzzy
msgid "Select"
msgstr "Valinnat"

#: gimp-keys.xml:150(key)
msgid "<ctrl/>T"
msgstr "<ctrl/>T"

#: gimp-keys.xml:150(action)
msgid "Toggle selections"
msgstr "Vaihda valinnat"

#: gimp-keys.xml:153(key)
msgid "<ctrl/>A"
msgstr "<ctrl/>A"

#: gimp-keys.xml:153(action)
msgid "Select all"
msgstr "Valitse kaikki"

#: gimp-keys.xml:156(key)
msgid "<shift/><ctrl/>A"
msgstr "<shift/><ctrl/>A"

#: gimp-keys.xml:156(action)
msgid "Select none"
msgstr "Poista valinnat"

#: gimp-keys.xml:159(key)
msgid "<ctrl/>I"
msgstr "<ctrl/>I"

#: gimp-keys.xml:159(action)
msgid "Invert selection"
msgstr "Käänteinen valinta"

#: gimp-keys.xml:162(key)
msgid "<shift/><ctrl/>L"
msgstr "<shift/><ctrl/>L"

#: gimp-keys.xml:162(action)
msgid "Float selection"
msgstr "Kelluva valinta"

#: gimp-keys.xml:165(key)
msgid "<shift/>V"
msgstr "<shift/>V"

#: gimp-keys.xml:165(action)
msgid "Path to selection"
msgstr "Polku valinnaksi"

#: gimp-keys.xml:171(title)
msgid "View"
msgstr "Näytä"

#: gimp-keys.xml:173(title)
msgid "Window"
msgstr "Ikkuna"

#: gimp-keys.xml:174(note)
msgid ""
"Menus can also be activated by Alt with the letter underscored in the menu "
"name."
msgstr ""
"Valikot voi aktivoida myös painamalla Alt + kirjain, joka on alleviivattu "
"valikon nimessä."

#: gimp-keys.xml:178(action)
msgid "Main Menu"
msgstr "Päävalikko"

#: gimp-keys.xml:182(action)
msgid "Drop-down Menu"
msgstr "Pudotusvalikko"

#: gimp-keys.xml:185(action)
msgid "Toggle fullscreen"
msgstr "Vaihda kokonäyttötilaan"

#: gimp-keys.xml:189(action)
msgid "Toggle the visibility of toolbox and dialogs docks"
msgstr ""

#: gimp-keys.xml:192(key)
msgid "<shift/>Q"
msgstr "<shift/>Q"

#: gimp-keys.xml:192(action)
msgid "Toggle quickmask"
msgstr "Kytke pikamaski päälle/pois"

#: gimp-keys.xml:195(action)
msgid "Close document window"
msgstr "Sulje dokumentti-ikkuna"

#: gimp-keys.xml:198(key)
#, fuzzy
msgid "<shift/>J"
msgstr "<shift/>O"

#: gimp-keys.xml:198(action)
#, fuzzy
msgid "Center image in window"
msgstr "Sovittaa kuvan ikkunaan"

#: gimp-keys.xml:201(key)
#, fuzzy
msgid "<shift/><ctrl/>J"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:201(action)
msgid "Fit image in window"
msgstr "Sovittaa kuvan ikkunaan"

#: gimp-keys.xml:206(title)
#: gimp-keys.xml:226(action)
#: gimp-keys.xml:463(action)
msgid "Zoom"
msgstr "Suurennus"

#: gimp-keys.xml:208(key)
msgid "+"
msgstr "+"

#: gimp-keys.xml:208(action)
#, fuzzy
msgid "Zoom In"
msgstr "Suurenna"

#: gimp-keys.xml:211(key)
msgid "-"
msgstr "-"

#: gimp-keys.xml:211(action)
#, fuzzy
msgid "Zoom Out"
msgstr "Pienennä"

#: gimp-keys.xml:214(key)
msgid "1"
msgstr "1"

#: gimp-keys.xml:214(action)
msgid "Zoom 1:1"
msgstr "Suhde 1:1"

#: gimp-keys.xml:217(key)
msgid "`"
msgstr ""

#: gimp-keys.xml:217(action)
msgid "Revert zoom"
msgstr ""

#: gimp-keys.xml:220(key)
#, fuzzy
msgid "<ctrl/>J"
msgstr "<ctrl/>N"

#: gimp-keys.xml:220(action)
msgid "Shrink wrap"
msgstr "Pienennä ikkuna"

#: gimp-keys.xml:222(note)
#, fuzzy
msgid "This fits the window to the image size."
msgstr "Sovittaa ikkunat kuvan kokoon."

#: gimp-keys.xml:231(title)
msgid "Flip and Rotate (0°)"
msgstr ""

#: gimp-keys.xml:233(key)
msgid "!"
msgstr ""

#: gimp-keys.xml:233(action)
msgid "Reset Flip and Rotate"
msgstr ""

#: gimp-keys.xml:238(title)
msgid "Scrolling (panning)"
msgstr "Vieritys (panorointi)"

#: gimp-keys.xml:240(action)
#: gimp-keys.xml:246(action)
msgid "Scroll canvas"
msgstr "Vieritä kangasta"

#: gimp-keys.xml:242(note)
#, fuzzy
msgid ""
"Scrolling by keys is accelerated, i.e. it speeds up when you press Shift"
"+arrows."
msgstr ""
"Vieritys näppäimillä on kiihdytetty, eli se nopeutuu kun painat Shift"
"+nuolinäppäin, tai hyppää reunoille Ctrl+nuolinäppäin."

#: gimp-keys.xml:249(action)
msgid "Scroll canvas vertically"
msgstr "Vieritä kangasta pystysuunnassa"

#: gimp-keys.xml:252(action)
msgid "Scroll canvas horizontally"
msgstr "Vieritä kangasta vaakasuunnassa"

#: gimp-keys.xml:257(title)
msgid "Rulers and Guides"
msgstr "Viivottimet ja apulinjat"

#: gimp-keys.xml:259(action)
msgid "Drag off a ruler to create guide"
msgstr "Raahaa viivainta luodaksesi apulinjan"

#: gimp-keys.xml:261(note)
msgid ""
"Drag off the horizontal or vertical ruler to create a new guide line. Drag a "
"guide line onto the ruler to delete it."
msgstr ""
"Raahaa vaaka- tai pystyviivaimesta pois päin luodaksesi uuden apulinjan. "
"Raahaa apulinja viivaimen päälle poistaaksesi sen."

#: gimp-keys.xml:265(action)
msgid "Drag a sample point out of the rulers"
msgstr "Raahaa otospiste ulos viivaimista"

#: gimp-keys.xml:268(key)
msgid "<shift/><ctrl/>R"
msgstr "<shift/><ctrl/>R"

#: gimp-keys.xml:268(action)
msgid "Toggle rulers"
msgstr "Kytke viivaimet päälle/pois"

#: gimp-keys.xml:271(key)
msgid "<shift/><ctrl/>T"
msgstr "<shift/><ctrl/>T"

#: gimp-keys.xml:271(action)
msgid "Toggle guides"
msgstr "Kytke apulinjat päälle/pois"

#: gimp-keys.xml:279(title)
msgid "Image"
msgstr ""

#: gimp-keys.xml:282(key)
msgid "<ctrl/>D"
msgstr "<ctrl/>D"

#: gimp-keys.xml:282(action)
msgid "Duplicate image"
msgstr "Kahdenna kuva"

#: gimp-keys.xml:286(action)
msgid "Image properties"
msgstr ""

#: gimp-keys.xml:292(title)
#: gimp-keys.xml:507(action)
msgid "Layers"
msgstr "Tasot"

#: gimp-keys.xml:295(key)
msgid "<shift/><ctrl/>N"
msgstr "<shift/><ctrl/>N"

#: gimp-keys.xml:295(action)
#, fuzzy
msgid "New layer"
msgstr "Uusi kuva"

#: gimp-keys.xml:298(key)
#, fuzzy
msgid "<shift/><ctrl/>D"
msgstr "<shift/><ctrl/>S"

#: gimp-keys.xml:298(action)
#, fuzzy
msgid "Duplicate layer"
msgstr "Kahdenna kuva"

#: gimp-keys.xml:302(action)
msgid "Select the layer above"
msgstr "Valitse yllä oleva taso"

#: gimp-keys.xml:306(action)
msgid "Select the layer below"
msgstr "Valitse alla oleva taso"

#: gimp-keys.xml:309(key)
msgid "<ctrl/>M"
msgstr "<ctrl/>M"

#: gimp-keys.xml:309(action)
msgid "Merge visible layers"
msgstr "Yhdistä näkyvät tasot"

#: gimp-keys.xml:312(key)
msgid "<ctrl/>H"
msgstr "<ctrl/>H"

#: gimp-keys.xml:312(action)
msgid "Anchor layer"
msgstr "Ankkuroi taso"

#: gimp-keys.xml:318(title)
#: gimp-keys.xml:474(action)
msgid "Toolbox"
msgstr "Työkalupakki"

#: gimp-keys.xml:320(title)
msgid "Tools"
msgstr "Työkalut"

#: gimp-keys.xml:322(key)
msgid "R"
msgstr "R"

#: gimp-keys.xml:323(action)
#, fuzzy
msgid "Rectangle Select"
msgstr "Suorakulmiovalinta"

#: gimp-keys.xml:326(key)
msgid "E"
msgstr "E"

#: gimp-keys.xml:327(action)
msgid "Ellipse Select"
msgstr "Ellipsivalinta"

#: gimp-keys.xml:330(key)
msgid "F"
msgstr "F"

#: gimp-keys.xml:331(action)
msgid "Free Select"
msgstr "Vapaa valinta"

#: gimp-keys.xml:334(key) gimp-keys.xml:462(key)
msgid "Z"
msgstr "Z"

#: gimp-keys.xml:335(action)
msgid "Fuzzy Select"
msgstr "Sumea valinta"

#: gimp-keys.xml:338(key)
msgid "<shift/>O"
msgstr "<shift/>O"

#: gimp-keys.xml:339(action)
msgid "Select By Color"
msgstr "Valitse värin mukaan"

#: gimp-keys.xml:342(key)
msgid "I"
msgstr "I"

#: gimp-keys.xml:343(action)
msgid "Intelligent Scissors"
msgstr ""

#: gimp-keys.xml:346(key)
msgid "<shift/>B"
msgstr "<shift/>B"

#: gimp-keys.xml:347(action)
msgid "Bucket Fill"
msgstr "Ämpäritäyttö"

#: gimp-keys.xml:350(key)
msgid "G"
msgstr ""

#: gimp-keys.xml:351(action)
#, fuzzy
msgid "Gradient"
msgstr "Liukuvärit"

#: gimp-keys.xml:354(key)
msgid "N"
msgstr "N"

#: gimp-keys.xml:355(action)
msgid "Pencil"
msgstr "Kynä"

#: gimp-keys.xml:358(key)
msgid "P"
msgstr "P"

#: gimp-keys.xml:359(action)
msgid "Paintbrush"
msgstr "Sivellin"

#: gimp-keys.xml:362(key)
msgid "<shift/>E"
msgstr "<shift/>E"

#: gimp-keys.xml:363(action)
msgid "Eraser"
msgstr "Pyyhekumi"

#: gimp-keys.xml:366(key)
msgid "A"
msgstr "A"

#: gimp-keys.xml:367(action)
msgid "Airbrush"
msgstr "Ruisku"

#: gimp-keys.xml:370(key)
msgid "K"
msgstr "K"

#: gimp-keys.xml:371(action)
msgid "Ink"
msgstr "Mustekynä"

#: gimp-keys.xml:374(key)
msgid "Y"
msgstr ""

#: gimp-keys.xml:375(action)
#, fuzzy
msgid "MyPaint Brush"
msgstr "Sivellin"

#: gimp-keys.xml:378(key)
msgid "C"
msgstr "C"

#: gimp-keys.xml:379(action)
msgid "Clone"
msgstr "Kloonaus"

#: gimp-keys.xml:382(key)
msgid "H"
msgstr ""

#: gimp-keys.xml:383(action)
msgid "Heal"
msgstr ""

#: gimp-keys.xml:386(key)
msgid "<shift/>U"
msgstr "<shift/>U"

#: gimp-keys.xml:387(action)
msgid "Blur/Sharpen"
msgstr "Sumenna/Terävöi"

#: gimp-keys.xml:390(key)
msgid "S"
msgstr "S"

#: gimp-keys.xml:391(action)
msgid "Smudge"
msgstr "Tuhri"

#: gimp-keys.xml:394(key)
msgid "<shift/>D"
msgstr "<shift/>D"

#: gimp-keys.xml:395(action)
msgid "Dodge/Burn"
msgstr "Varjosta/lisävalota"

#: gimp-keys.xml:398(key)
msgid "Q"
msgstr ""

#: gimp-keys.xml:399(action)
msgid "Alignment"
msgstr ""

#: gimp-keys.xml:402(key)
msgid "M"
msgstr "M"

#: gimp-keys.xml:403(action)
msgid "Move"
msgstr "Siirto"

#: gimp-keys.xml:406(key)
msgid "<shift/>C"
msgstr "<shift/>C"

#: gimp-keys.xml:407(action)
msgid "Crop"
msgstr ""

#: gimp-keys.xml:410(key)
msgid "<shift/>R"
msgstr "<shift/>R"

#: gimp-keys.xml:411(action)
msgid "Rotate"
msgstr "Kierto"

#: gimp-keys.xml:414(key)
msgid "<shift/>S"
msgstr "<shift/>S"

#: gimp-keys.xml:415(action)
msgid "Scale"
msgstr "Skaalaus"

#: gimp-keys.xml:418(key)
#, fuzzy
msgid "<shift/>H"
msgstr "<shift/>O"

#: gimp-keys.xml:419(action)
msgid "Shear"
msgstr "Viistota"

#: gimp-keys.xml:422(key)
msgid "<shift/>P"
msgstr "<shift/>P"

#: gimp-keys.xml:423(action)
msgid "Perspective"
msgstr "Perspektiivi"

#: gimp-keys.xml:426(key)
msgid "<shift/>T"
msgstr "<shift/>T"

#: gimp-keys.xml:427(action)
msgid "Unified Transform"
msgstr ""

#: gimp-keys.xml:430(key)
msgid "<shift/>L"
msgstr "<shift/>L"

#: gimp-keys.xml:431(action)
msgid "Handle Transform"
msgstr ""

#: gimp-keys.xml:434(key)
msgid "<shift/>F"
msgstr "<shift/>F"

#: gimp-keys.xml:435(action)
msgid "Flip"
msgstr "Peilikuvatyökalu"

#: gimp-keys.xml:438(key)
#, fuzzy
msgid "<shift/>G"
msgstr "<shift/>O"

#: gimp-keys.xml:439(action)
msgid "Cage Transform"
msgstr ""

#: gimp-keys.xml:442(key)
msgid "W"
msgstr ""

#: gimp-keys.xml:443(action)
msgid "Warp Transform"
msgstr ""

#: gimp-keys.xml:446(key)
msgid "B"
msgstr "B"

#: gimp-keys.xml:447(action)
msgid "Paths"
msgstr "Polut"

#: gimp-keys.xml:450(key)
msgid "T"
msgstr "T"

#: gimp-keys.xml:451(action)
msgid "Text"
msgstr "Teksti"

#: gimp-keys.xml:454(key)
msgid "O"
msgstr "O"

#: gimp-keys.xml:455(action)
msgid "Color Picker"
msgstr "Värinvalitsin"

#: gimp-keys.xml:458(key)
#, fuzzy
msgid "<shift/>M"
msgstr "<shift/>O"

#: gimp-keys.xml:459(action)
msgid "Measure"
msgstr ""

#: gimp-keys.xml:465(note)
msgid "Double click on the tool buttons opens the Tool Options dialog."
msgstr ""
"Kaksoisnapsautus työkalupainikkeiden päällä avaan Työkaluasetukset-"
"valintaikkunan."

#: gimp-keys.xml:471(title)
msgid "Context"
msgstr "Konteksti"

#: gimp-keys.xml:473(key)
#, fuzzy
msgid "<Ctrl/>B"
msgstr "<ctrl/>N"

#: gimp-keys.xml:477(key)
msgid "D"
msgstr "D"

#: gimp-keys.xml:478(action)
msgid "Default Colors"
msgstr "Oletusvärit"

#: gimp-keys.xml:481(key)
msgid "X"
msgstr "X"

#: gimp-keys.xml:482(action)
msgid "Swap Colors"
msgstr "Vaihda värejä"

#: gimp-keys.xml:484(note)
msgid "Click on the colors to change the colors."
msgstr "Napsauta värejä vaihtaaksesi värejä"

#: gimp-keys.xml:492(title)
msgid "Filters"
msgstr "Suotimet"

#: gimp-keys.xml:495(key)
msgid "<ctrl/>F"
msgstr "<ctrl/>F"

#: gimp-keys.xml:495(action)
msgid "Repeat last filter"
msgstr "Toista viimeisin suodin"

#: gimp-keys.xml:498(key)
msgid "<shift/><ctrl/>F"
msgstr "<shift/><ctrl/>F"

#: gimp-keys.xml:498(action)
msgid "Reshow last filter"
msgstr "Näytä viimeisin suodin uudelleen"

#: gimp-keys.xml:504(title)
#, fuzzy
msgid "Windows"
msgstr "Ikkuna"

#: gimp-keys.xml:507(key)
msgid "<ctrl/>L"
msgstr "<ctrl/>L"

#: gimp-keys.xml:510(key)
msgid "<shift/><ctrl/>B"
msgstr "<shift/><ctrl/>B"

#: gimp-keys.xml:510(action)
msgid "Brushes"
msgstr "Siveltimet"

#: gimp-keys.xml:513(key)
msgid "<shift/><ctrl/>P"
msgstr "<shift/><ctrl/>P"

#: gimp-keys.xml:513(action)
msgid "Patterns"
msgstr "Kuviot"

#: gimp-keys.xml:516(key)
msgid "<ctrl/>G"
msgstr "<ctrl/>G"

#: gimp-keys.xml:516(action)
msgid "Gradients"
msgstr "Liukuvärit"

#: gimp-keys.xml:518(note)
msgid ""
"These open a new dialog window if it wasn't open yet, otherwise the "
"corresponding dialog gets focus."
msgstr ""
"Avaa uuden valintaikkunan, jos se ei ole vielä avoinna. Muuten valintaikkuna "
"valitaan aktiiviseksi."

#: gimp-keys.xml:524(title)
msgid "Within a Dialog"
msgstr "Valintaikkunassa"

#: gimp-keys.xml:526(action)
msgid "Set the new value"
msgstr "Aseta uusi arvo"

#: gimp-keys.xml:528(note)
msgid ""
"This accepts the new value you typed in a text field and returns focus to "
"canvas."
msgstr ""
"Tämä hyväksyy tekstikenttään kirjoitetun uuden arvon ja palauttaa "
"kohdistuksen kankaalle."

#: gimp-keys.xml:533(action)
msgid "Activate current button or list"
msgstr "Aktivoi nykyinen painike tai luettelo"

#: gimp-keys.xml:537(title)
#, fuzzy
msgid "Within a multi-tab dialog"
msgstr "Tiedostovalintaikkunassa"

#: gimp-keys.xml:540(action)
msgid "Switch tabs up"
msgstr ""

#: gimp-keys.xml:544(action)
msgid "Switch tabs down"
msgstr ""

#: gimp-keys.xml:549(title)
msgid "Within a File Dialog"
msgstr "Tiedostovalintaikkunassa"

#: gimp-keys.xml:551(action)
msgid "Up-Folder"
msgstr "Yläkansio"

#: gimp-keys.xml:554(action)
msgid "Down-Folder"
msgstr "Alakansio"

#: gimp-keys.xml:557(action)
msgid "Home-Folder"
msgstr "Kotikansio"

#: gimp-keys.xml:560(action)
msgid "Close Dialog"
msgstr "Sulje valintaikkuna"

#: gimp-keys.xml:565(title)
#: gimp-keys.xml:569(action)
msgid "Help"
msgstr "Ohje"

#: gimp-keys.xml:573(action)
msgid "Context Help"
msgstr "Sisältöavustaja"

#: gimp-keys.xml:576(key)
msgid "/"
msgstr ""

#: gimp-keys.xml:577(action)
msgid "Search and run a command"
msgstr ""

#: gimp-keys.xml:585(title)
msgid "Zoom tool"
msgstr "Suurennustyökalu"

#: gimp-keys.xml:588(action)
msgid "Zoom in"
msgstr "Suurenna"

#: gimp-keys.xml:591(action)
msgid "Zoom out"
msgstr "Pienennä"

#: gimp-keys.xml:595(action)
#, fuzzy
msgid "Zoom in inside the area"
msgstr "Suurenna alue"

#: gimp-keys.xml:599(action)
#, fuzzy
msgid "Zoom out inside the area"
msgstr "Suurenna alue"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: gimp-keys.xml:0(None)
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Elias Julkunen https://launchpad.net/~eliasj\n"
"  Timo Jyrinki https://launchpad.net/~timo-jyrinki"

#~ msgid "Scissors"
#~ msgstr "Sakset"

#~ msgid "Crop and Resize"
#~ msgstr "Rajaa ja muuta kokoa"

#~ msgid "L"
#~ msgstr "L"

#~ msgid "Blend"
#~ msgstr "Liukuväri"

#~ msgid "Save under a new name"
#~ msgstr "Tallenna uudella nimellä"

#~ msgid "Export ..."
#~ msgstr "Vie ..."

#~ msgid "Dialogs"
#~ msgstr "Valintaikkunat"

#~ msgid "Tool-Options"
#~ msgstr "Työkaluasetukset"

#~ msgid "Palettes"
#~ msgstr "Paletit"

#~ msgid "<shift/><ctrl/>I"
#~ msgstr "<shift/><ctrl/>I"

#~ msgid "Info window"
#~ msgstr "Tietoikkuna"

#~ msgid "Navigation window"
#~ msgstr "Navigaatioikkuna"

#~ msgid "Jump to next widget"
#~ msgstr "Siirry seuraavaan elementtiin"

#~ msgid "Jump to previous widget"
#~ msgstr "Siirry edelliseen elementtiin"

#~ msgid "In a multi-tab dialog, switch tabs"
#~ msgstr "Siirry välilehtien välille monivälilehtisessä valintaikkunassa"

#~ msgid "Open Location"
#~ msgstr "Avaa sijainti"

#~ msgid "<ctrl/>K"
#~ msgstr "<ctrl/>K"

#~ msgid "Clears selection"
#~ msgstr "Tyhjentää valinnan"

#~ msgid "Named copy selection"
#~ msgstr "Nimetty kopiointivalinta"

#~ msgid "<shift/><ctrl/>X"
#~ msgstr "<shift/><ctrl/>X"

#~ msgid "Named cut selection"
#~ msgstr "Nimetty leikkausvalinta"

#~ msgid "Named paste clipboard"
#~ msgstr "Nimetty liitä-leikepöytä"

#~| msgid "Clears selection"
#~ msgid "Erase selection"
#~ msgstr "Pyyhi valittu"

#~ msgid "Select the first layer"
#~ msgstr "Valitse ensimmäinen taso"

#~ msgid "Select the last layer"
#~ msgstr "Valitse viimeinen taso"

#~ msgid "Plug-ins"
#~ msgstr "Liitännäiset"

#~ msgid "V"
#~ msgstr "V"

#~ msgid "Convolve"
#~ msgstr "Konvoluutio"
