Source: gimp-help
Section: doc
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Ari Pollak <ari@debian.org>, @GNOME_TEAM@
Build-Depends: debhelper-compat (= 12),
               gnome-pkg-tools,
               pkg-config
Build-Depends-Indep: xsltproc (>= 1.1.12), docbook-xml, docbook-xsl, python3:any, python3-libxml2
Build-Conflicts: dblatex, docbook2odf
Standards-Version: 4.2.1
Homepage: https://www.gimp.org
Vcs-Git: https://salsa.debian.org/gnome-team/gimp-help.git
Vcs-Browser: https://salsa.debian.org/gnome-team/gimp-help

Package: gimp-help-common
Architecture: all
Depends: ${misc:Depends}
Description: Data files for the GIMP documentation
 This package contains necessary files common to all GIMP help
 packages, such as graphics and screenshots.

Package: gimp-help-ca
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Catalan)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Catalan.

Package: gimp-help-cs
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Czech)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Czech.

Package: gimp-help-da
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Danish)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Danish.

Package: gimp-help-de
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (German)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in German.

Package: gimp-help-el
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Greek)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Greek.

Package: gimp-help-en
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (English)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in English.

Package: gimp-help-en-gb
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (British English)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in British English.

Package: gimp-help-es
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Spanish)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Spanish.

Package: gimp-help-fa
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Farsi)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Farsi.

Package: gimp-help-fi
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Finnish)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Finnish.

Package: gimp-help-fr
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (French)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in French.

Package: gimp-help-hr
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Croatian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Croatian.

Package: gimp-help-hu
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Hungarian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Hungarian.

Package: gimp-help-it
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Italian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Italian.

Package: gimp-help-ja
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Japanese)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Japanese.

Package: gimp-help-ko
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Korean)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Korean.

Package: gimp-help-lt
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Lithuanian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Lithuanian.

Package: gimp-help-nl
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Dutch)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Dutch.

Package: gimp-help-nn
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Norwegian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Norwegian.

Package: gimp-help-pt
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Portuguese)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Portuguese.

Package: gimp-help-pt-br
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Brazilian Portuguese)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Brazilian Portuguese.

Package: gimp-help-ro
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Romanian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Romanian.

Package: gimp-help-ru
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Russian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Russian.

Package: gimp-help-sl
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Slovenian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Slovenian.

Package: gimp-help-sv
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Swedish)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Swedish.

Package: gimp-help-uk
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Ukrainian)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Ukrainian.

Package: gimp-help-zh-cn
Architecture: all
Depends: ${misc:Depends}, gimp-help-common (= ${source:Version}), gimp-helpbrowser | www-browser
Provides: gimp-help
Enhances: gimp
Description: Documentation for the GIMP (Simplified Chinese)
 This package contains the documentation files for the GIMP designed for use
 with the internal GIMP help browser or external web browsers.
 .
 This package contains the documentation for the GIMP in Simplified Chinese.
